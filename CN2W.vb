Public Class CN2W
    ' ----------------------------------
    ' Class CN2W_VB6
    ' By Harvey Triana
    ' ----------------------------------

    Private Const ZERO As String = "Zero"

    Private IntegerPart As String
    Private RealPart As String
    Private m_DecimalSeparator As String
    Private m_GroupSeparator As String

    Private aT0 As String() = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"}
    Private aT1 As String() = {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"}
    Private aT2 As String() = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"}
    Private aT3 As String() = {"", "", " Thousand ", " Million ", " Billion ", " Trillion ", "", "", "", ""}

    Public Function IntegerToWords(ByVal Number As String) As String
        Call RealToWords(Number)
        IntegerToWords = IntegerPart
    End Function

    Public Function RealToWords(ByVal Number As String) As String
        Dim DotPos As Long

        IntegerPart = ZERO
        RealPart = ZERO

        If IsNumeric(Number) Then
            DotPos = InStr(Number, m_DecimalSeparator)
            Number = Replace$(Number, m_GroupSeparator, "")
            If DotPos Then
                IntegerPart = ToWords(Int(Left$(Number, DotPos - 1)))
                RealPart = ToWords(Int(Mid$(Number, DotPos + 1)))
            Else
                IntegerPart = ToWords(Int(Number))
            End If
        End If

        RealToWords = IntegerPart & " Dot " & RealPart
    End Function

    Public Function CurrencyToWords(ByVal Number As String, ByVal MoneyName As String) As String
        Dim s As String

        IntegerPart = ZERO
        RealPart = ZERO

        If IsNumeric(Number) Then
            ' round to money number
            s = RealToWords(RoundNumberString(CDbl(Number), 2))
        End If
        ' add money text
        If IntegerPart = ZERO Then IntegerPart = "No"
        Select Case RealPart
            Case ZERO : RealPart = "And No Cents"
            Case "One" : RealPart = "And One Cent"
            Case Else : RealPart = "And " & RealPart & " Cents"
        End Select
        CurrencyToWords = IntegerPart & " " & MoneyName & " " & RealPart
    End Function

    Private Function ToWords(ByVal Number As Long) As String
        Dim s As String = String.Empty
        Dim d As String = String.Empty
        Dim r As String = String.Empty
        Dim Count As Long

        s = CStr(Number)
        Count = 1
        Do Until Len(s) = 0
            ' convert last 3 digits of s to english words
            If Len(s) < 3 Then
                d = ConvertHundreds(s)
            Else
                d = ConvertHundreds(Right$(s, 3))
            End If
            If Len(d) > 0 Then r = d & aT3(Count) & r
            If Len(s) > 3 Then
                ' remove last 3 converted digits from s.
                s = Left$(s, Len(s) - 3)
            Else
                s = ""
            End If
            Count = Count + 1
        Loop
        If Len(r) = 0 Then r = ZERO
        ToWords = Trim$(r)
    End Function

    Private Function ConvertHundreds(ByVal pNumber As String) As String
        Dim r As String

        If Not Int(pNumber) = 0 Then
            ' append leading zeros to number.
            pNumber = Right$("000" & pNumber, 3)
            ' do we have a hundreds place digit to convert?
            If Not Left$(pNumber, 1) = "0" Then
                r = ConvertDigit(Left$(pNumber, 1)) & " Hundred "
            End If
            ' do we have a tens place digit to convert?
            If Len(pNumber) >= 2 Then
                If Not Mid$(pNumber, 2, 1) = "0" Then
                    r = r & ConvertTens(Mid$(pNumber, 2))
                Else
                    r = r & ConvertDigit(Mid$(pNumber, 3))
                End If
            End If
            ConvertHundreds = Trim(r)
        End If
    End Function

    Private Function ConvertTens(ByVal pTens As String) As String
        Dim r As String
        ' is value between 10 and 19?
        If Int(Left$(pTens, 1)) = 1 Then
            r = aT1(Int(pTens) - 10)
        Else
            ' otherwise it's between 20 and 99.
            r = aT2(Int(Left$(pTens, 1)) - 2) & " "
            ' convert ones place digit
            r = r & ConvertDigit(Right$(pTens, 1))
        End If
        ConvertTens = r
    End Function

    Private Function ConvertDigit(ByVal pNumber As String) As String
        If pNumber = "0" Then
            ConvertDigit = ""
        Else
            ConvertDigit = aT0(Int(pNumber) - 1)
        End If
    End Function

    Private Function RoundNumberString(ByVal Number As Double, ByVal Decimals As Long) As String
        Dim s As String
        Dim r As Double

        ' round first
        r = Int(Number * 10 ^ Decimals + 0.5) / (10 ^ Decimals)
        ' complete with zeros
        s = CStr(r)
        If InStr(s, m_DecimalSeparator) = 0 Then s = s & m_DecimalSeparator
        Do While Len(Mid$(s, InStr(s, m_DecimalSeparator))) <= Decimals
            s = s & "0"
        Loop
        RoundNumberString = s
    End Function

    Public Sub New()
        IntegerPart = ZERO
        RealPart = ZERO
        'aT0 = ("One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine")
        'aT1 = Array("Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen")
        'aT2 = Array("Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety")
        'aT3 = Array("", "", " Thousand ", " Million ", " Billion ", " Trillion ", "", "", "", "")

        '$GUILLE: 06/Ago/2006 14.53
        If InStr(Format$(1.5, "#.#"), ",") Then
            m_DecimalSeparator = ","
            m_GroupSeparator = "."
        Else
            m_DecimalSeparator = "."
            m_GroupSeparator = ","
        End If
    End Sub
    Public ReadOnly Property DecimalSeparator() As String
        Get
            DecimalSeparator = m_DecimalSeparator
        End Get
    End Property

    Public ReadOnly Property GroupSeparator() As String
        Get
            GroupSeparator = m_GroupSeparator
        End Get
    End Property

    
    Public Function FigureToWord(ByRef mfigure As Object) As String
        On Error GoTo ErrHandler
        If Len(LTrim(RTrim(mfigure))) = 0 Then
            Exit Function
        End If
        Dim i, j As Short
        Dim ch, tmp As String
        j = 0
        For i = 1 To Len(mfigure)
            ch = Mid(mfigure, i, 1)
            If ch < "0" Or ch > "9" Then
                If ch <> "." Then
                    FigureToWord = "Invalid"
                    Exit Function
                Else
                    j = j + 1
                End If
            End If
        Next i

        If j > 1 Then
            FigureToWord = "Invalid"
            Exit Function
        End If


        ' Remove front CERO if any
        For i = 1 To Len(mfigure)
            ch = Mid(mfigure, i, 1)
            If Val(ch) > 0 Then
                Exit For
            ElseIf ch = "." Then
                Exit For
            End If
        Next i

        If i > 1 Then
            mfigure = Strings.Right(mfigure, Len(mfigure) - i + 1)
        End If


        If mfigure = "" Or mfigure = "." Then
            FigureToWord = "CERO"
            Exit Function
        End If

        Dim mWord, mstring, mdummypass As String
        mfigure = Format(CDec(mfigure), "####0.00")

        ' Convert it to string for string manipulation
        mstring = CStr(mfigure)

        ' Make it valid upto Millon, i.e. 9 digits before decimal point.

        If Len(mstring) > 12 Then
            FigureToWord = "Limit to 12 digits only, including two digits" & vbCrLf & "after decimal (implicit or explicit)."
            Exit Function
        Else
            If Len(mstring) < 12 Then ' Make up to uniform length of 12 digits
                mstring = Space(12 - Len(mstring)) & mstring
            End If
        End If

        mWord = ""

        ' For first 3 digits
        If Strings.Left(mstring, 3) <> "   " And Strings.Left(mstring, 3) <> "000" Then
            mdummypass = doSegment(Strings.Left(mstring, 3))
            mWord = IIf(mdummypass = "UNO", "UN MILLON ", mdummypass & " MILLONES ")
        End If

        If Mid(mstring, 4, 3) <> "   " And Mid(mstring, 4, 3) <> "000" Then
            mdummypass = doSegment(Mid(mstring, 4, 3))
            mWord = mWord & IIf(mdummypass = "UNO", "", mdummypass) & " MIL "
        End If

        ' For last 3
        If Mid(mstring, 7, 3) <> "   " And Mid(mstring, 7, 3) <> "000" Then
            mdummypass = doSegment(Mid(mstring, 7, 3))
            If mWord <> "" Then
                mWord = mWord & mdummypass
            Else
                mWord = mdummypass
            End If
        End If

        If Len(mWord) = 0 Then
            mWord = "CERO"
        End If

        If Val(Strings.Right(mstring, 2)) <> 0 Then
            If mWord <> "CERO" Then
                mWord = mWord & Space(1) & "CON" & Space(1) & Strings.Right(mstring, 2) & "/100"
            Else
                mWord = Strings.Right(mstring, 2) & "/100"
            End If
            'Else '(For this posting of screen display, ignore decimal if CERO after decimal point)
        End If

        FigureToWord = mWord
        Exit Function

ErrHandler:
        FigureToWord = "(Error occured)"
    End Function



    Private Function doSegment(ByRef m3digits As String) As String

        On Error GoTo ErrHandler
        Dim m100, mupto19, mupto90, mupto700 As String
        Dim mnum, mlastnum As Short

        mupto700 = "CIENTO                                      QUINIENTOS            SETECIENTOS           NOVECIENTOS"
        'mupto700 = "CIENTO                                      QUINIENTOS            SETECIENTOS"
        mupto19 = "UNO       DOS       TRES      CUATRO    CINCO     " & "SEIS      SIETE     OCHO      NUEVE     DIEZ      " & "ONCE      DOCE      TRECE     CATORCE   QUINCE    " & "DIECISEIS DIECISIETEDIECIOCHO DIECINUEVE"
        mupto90 = "VEINTE   TREINTA  CUARENTA CINCUENTA SESENTA  SETENTA  " & "OCHENTA  NOVENTA "
        m100 = ""

        If Trim(Strings.Left(m3digits, 1)) = "" Then
            m100 = ""
        Else
            mnum = CShort(Strings.Left(m3digits, 1))
            If mnum <> 0 Then
                If CDbl(m3digits) = 100 Then
                    m100 = "CIEN"
                ElseIf mnum = 1 Or mnum = 5 Or mnum = 7 Or mnum = 9 Then
                    m100 = Trim(Mid(mupto700, (mnum - 1) * 11 + 1, 11))
                Else
                    m100 = Trim(Mid(mupto19, (mnum - 1) * 10 + 1, 10)) & "CIENTOS"
                    'Space(1) & "CIENTOS"
                End If
            Else
                m100 = ""
            End If
        End If

        If Trim(Strings.Right(m3digits, 2)) = "" Then
            m100 = ""
        Else
            mnum = CShort(Strings.Right(m3digits, 2))
            If mnum > 0 And mnum < 20 Then
                'm100 = m100 & Space(1) & Trim(Mid(mupto19, (mnum - 1) * 10 + 1, 10))
                m100 = m100 & Trim(Mid(mupto19, (mnum - 1) * 10 + 1, 10))
            End If
            If mnum >= 20 Then
                m100 = Trim(m100 & Space(1) & Trim(Mid(mupto90, (Int(mnum / 10) - 2) * 9 + 1, 9)))
                mlastnum = CShort(Strings.Right(m3digits, 1))
                If mlastnum > 0 Then
                    m100 = m100 & Space(1) & "Y" & Space(1) & Trim(Mid(mupto19, (mlastnum - 1) * 10 + 1, 9))
                End If
            End If
        End If
        doSegment = m100
        Exit Function

ErrHandler:
        doSegment = "(Error occured)"
    End Function

End Class
