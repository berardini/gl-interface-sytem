Imports System.windows.forms.Application
Imports System.Object
Imports System.IO
Imports System.IO.StreamReader
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class frmCheckGLImport

    Inherits System.Windows.Forms.Form

    Dim fName As String
    Dim companybreak As String
    Public DepTotal As Double


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CmdExit As System.Windows.Forms.Button
    Friend WithEvents CmdProcess As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblBatchNbr As System.Windows.Forms.Label
    Friend WithEvents txtBatchNbr As System.Windows.Forms.TextBox
    Friend WithEvents txtBatchTotal As System.Windows.Forms.TextBox
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents lblBatchTotal As System.Windows.Forms.Label
    Friend WithEvents txtBatchCheckTotal As System.Windows.Forms.TextBox
    Friend WithEvents lblBatchCheckTotal As System.Windows.Forms.Label
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CmdProcess = New System.Windows.Forms.Button
        Me.CmdExit = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.txtBatchCheckTotal = New System.Windows.Forms.TextBox
        Me.lblBatchCheckTotal = New System.Windows.Forms.Label
        Me.txtBatchTotal = New System.Windows.Forms.TextBox
        Me.lblBatchTotal = New System.Windows.Forms.Label
        Me.txtBatchNbr = New System.Windows.Forms.TextBox
        Me.lblBatchNbr = New System.Windows.Forms.Label
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdProcess
        '
        Me.CmdProcess.Location = New System.Drawing.Point(104, 144)
        Me.CmdProcess.Name = "CmdProcess"
        Me.CmdProcess.Size = New System.Drawing.Size(88, 24)
        Me.CmdProcess.TabIndex = 3
        Me.CmdProcess.Text = "&Process"
        '
        'CmdExit
        '
        Me.CmdExit.Location = New System.Drawing.Point(200, 144)
        Me.CmdExit.Name = "CmdExit"
        Me.CmdExit.Size = New System.Drawing.Size(88, 24)
        Me.CmdExit.TabIndex = 4
        Me.CmdExit.Text = "E&xit"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdPrint)
        Me.GroupBox1.Controls.Add(Me.txtBatchCheckTotal)
        Me.GroupBox1.Controls.Add(Me.lblBatchCheckTotal)
        Me.GroupBox1.Controls.Add(Me.txtBatchTotal)
        Me.GroupBox1.Controls.Add(Me.lblBatchTotal)
        Me.GroupBox1.Controls.Add(Me.txtBatchNbr)
        Me.GroupBox1.Controls.Add(Me.lblBatchNbr)
        Me.GroupBox1.Controls.Add(Me.CmdProcess)
        Me.GroupBox1.Controls.Add(Me.CmdExit)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 200)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        '
        'cmdPrint
        '
        Me.cmdPrint.Location = New System.Drawing.Point(8, 144)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(88, 24)
        Me.cmdPrint.TabIndex = 32
        Me.cmdPrint.Text = "&Print"
        '
        'txtBatchCheckTotal
        '
        Me.txtBatchCheckTotal.Location = New System.Drawing.Point(144, 64)
        Me.txtBatchCheckTotal.Name = "txtBatchCheckTotal"
        Me.txtBatchCheckTotal.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchCheckTotal.TabIndex = 1
        Me.txtBatchCheckTotal.Text = ""
        '
        'lblBatchCheckTotal
        '
        Me.lblBatchCheckTotal.Location = New System.Drawing.Point(16, 64)
        Me.lblBatchCheckTotal.Name = "lblBatchCheckTotal"
        Me.lblBatchCheckTotal.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchCheckTotal.TabIndex = 31
        Me.lblBatchCheckTotal.Text = "Total Checks"
        '
        'txtBatchTotal
        '
        Me.txtBatchTotal.Location = New System.Drawing.Point(144, 96)
        Me.txtBatchTotal.Name = "txtBatchTotal"
        Me.txtBatchTotal.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchTotal.TabIndex = 2
        Me.txtBatchTotal.Text = ""
        '
        'lblBatchTotal
        '
        Me.lblBatchTotal.Location = New System.Drawing.Point(16, 96)
        Me.lblBatchTotal.Name = "lblBatchTotal"
        Me.lblBatchTotal.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchTotal.TabIndex = 29
        Me.lblBatchTotal.Text = "Total Check Amount"
        '
        'txtBatchNbr
        '
        Me.txtBatchNbr.Location = New System.Drawing.Point(144, 32)
        Me.txtBatchNbr.Name = "txtBatchNbr"
        Me.txtBatchNbr.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchNbr.TabIndex = 0
        Me.txtBatchNbr.Text = ""
        '
        'lblBatchNbr
        '
        Me.lblBatchNbr.Location = New System.Drawing.Point(16, 32)
        Me.lblBatchNbr.Name = "lblBatchNbr"
        Me.lblBatchNbr.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchNbr.TabIndex = 25
        Me.lblBatchNbr.Text = "Batch Number"
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 218)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(322, 22)
        Me.StatusBar1.TabIndex = 27
        '
        'frmCheckGLImport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(322, 240)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCheckGLImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LP Check GL Import"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Initialize()
    End Sub

    Private Sub Initialize()
        StatusBar1.Text = ""
        DBPath = Application.StartupPath & "\data\GLACTCodes.mdb"
    End Sub

    Private Sub CmdProcess_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdProcess.Click

        Dim SourceFile, DestinationFile As String
        Dim i As Integer
        Dim j As Integer
        Dim CrRptCtr As Integer
        Dim TmpAccount As String
        Dim StrSQL As String
        Dim BatchNumberTrnQty As Long
        Dim BatchNumberTotalCR As Double
        Dim BatchNumberTotalDR As Double
        Dim TotalCheckAmount As Double
        Dim TotalCheck As Long
        Dim SQLString As String
        Dim BrkCheckNumber As String
        Dim n2w As CN2W




        TmpAccount = ""
        BatchNumberTotalCR = 0
        BatchNumberTotalDR = 0
        TotalCheckAmount = 0
        TotalCheck = 0

        If txtBatchNbr.Text = "" Then
            MsgBox("Enter a BatchNumber", vbOKOnly + vbExclamation)
            txtBatchNbr.Focus()
            Exit Sub
        End If

        If txtBatchCheckTotal.Text = "" Then
            MsgBox("Enter the Total Check", vbOKOnly + vbExclamation)
            txtBatchCheckTotal.Focus()
            Exit Sub
        End If

        If txtBatchTotal.Text = "" Then
            MsgBox("Enter the Total Check Amount", vbOKOnly + vbExclamation)
            txtBatchTotal.Focus()
            Exit Sub
        End If


        'Define SQL Connections
        Dim connInput As New SqlConnection



        'Define Data Adapter
        Dim Adp As SqlDataAdapter

        ' Define Data Set 
        Dim ds As New DataSet



        Try

            ' Assign SQL Connection Strings
            connInput.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("DBConnectionStringInput")


            StrSQL = "SELECT CHEK.BATCH_NUMBER, CHEK.CHECK_TRANS_DATE, CHEK.CHECK_NUMBER, CHEK.CHECK_STATUS, " & _
                     "CAST(CHEK.VENDOR_ID AS CHAR(12)) AS POLNUM, CHEK.COMBINED_CHECK_AMT, CHEK.ACTG_CONTROL_NUMBR, CHEK.CONTROL_NO,CHEK.OFFSET_ACCOUNT_1, " & _
                     "CHEK.EFFECT_DATE AS EFF_DATE,ACTG.EFFECTIVE_DATE AS ACTG_EFF_DATE,ACTG.DEBIT_ACCOUNT,ACTG.CREDIT_ACCOUNT, " & _
                     "ACTG.DEBIT_CODE,ACTG.CREDIT_CODE,ACTG.REVERSAL_CODE, " & _
                     "CASE WHEN NOT  (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "ELSE '00000000000' END AS  DEBIT_TRANS_AMT, " & _
                     "CASE WHEN (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "ELSE '00000000000' END AS  CREDIT_TRANS_AMT, " & _
                     "CAST(NAM1.SOC_SEC_NUMBER AS CHAR(12)) AS SSN, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS PAYOR, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS INSURED " & _
                     "FROM dbo.PCHEK AS CHEK " & _
                     "LEFT JOIN PACTG AS ACTG ON (ACTG.ACTG_KEY6 LIKE CHEK.COMPANY_CODE + CAST(CHEK.VENDOR_ID AS CHAR(12)) + CAST(CHEK.EFFECT_DATE AS CHAR(8)) + '%') " & _
                     "LEFT JOIN PNAME AS NAM1 ON (CHEK.NAME_ID = NAM1.NAME_ID) " & _
                     "WHERE CHEK.BATCH_NUMBER =" & "'" & txtBatchNbr.Text & "'" & _
                     "ORDER BY CHECK_NUMBER"

            '"CAST(RTRIM(NAME1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAME1.INDIVIDUAL_LAST) AS CHAR(30)) AS INSURED, " & _

            '"ISNULL(SUBSTRING(ADDR.ADDR_LINE_1, 1, 30),' ') AS ADD1, " & _
            '"ISNULL(ADDR.ADDR_LINE_2,' ') AS ADD2, " & _
            '"ISNULL(ADDR.ADDR_LINE_3,' ') AS ADD3, " & _
            '"ISNULL(SUBSTRING(ADDR.CITY, 1,15),' ') AS CITY, " & _
            '"ISNULL(ADDR.STATE,' ') AS ST, " & _
            '"ISNULL(ADDR.ZIP,' ') AS ZIP, " & _
            '"ISNULL(ADDR.ZIP_EXTENSION,' ') AS P4, " & _

            '' "LEFT JOIN PRELA AS RELA ON ('01' + CHEK.VENDOR_ID = RELA.IDENTIFYING_ALPHA and " & _
            ''                             "RELA.RELATE_CODE = 'IN' AND " & _
            ''                             "RELA.BENEFIT_SEQ_NUMBER= 1) " & _
            ''"LEFT JOIN PNAME AS NAME1 ON (NAME1.NAME_KEY0 = RELA.RELA_KEY0) " & _
            ''"LEFT JOIN PNALK AS NALK ON (NALK.NAME_ID = NAME1.NAME_ID  AND " & _
            ''                             "NALK.COMPANY_CODE  = '' AND " & _
            ''                             "NALK.POLICY_NUMBER = '') " & _
            ' "LEFT JOIN PADDR AS ADDR ON ADDR_KEY0 = CAST((RIGHT('00000000'+ RTRIM(NALK.ADDRESS_ID), 8)) AS BINARY(8)) " & _

            '"WHERE CHEK.BATCH_NUMBER =" & "'" & txtBatchNbr.Text & "'" & "AND ACTG.CREDIT_CODE IN (92, 93,13,1020,560,561,840) AND ACTG.REVERSAL_CODE = ' ' " & _

            'Execute Data Adapter

            StatusBar1.Text = "Performing Batch Query"

            Adp = New SqlDataAdapter(StrSQL, connInput)
            Adp.Fill(ds)

            'Rutina q verifica si hubo transacciones para ese BatchNumber
            If ds.Tables(0).Rows.Count = 0 Then
                MsgBox("No Transactions for this BatchNumber", vbOKOnly + vbExclamation)
                Exit Sub
            End If



            'Cantidad de Cheques transacciones del batch
            BatchNumberTrnQty = ds.Tables(0).Rows.Count
            ''(92, 93,13,1020,560,561,840)
            For i = 0 To ds.Tables(0).Rows.Count - 1
                'if a�adido por modificacion en SQL Query
                If (Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "840") And _
                   Trim(ds.Tables(0).Rows(i).Item("REVERSAL_CODE")) = "" Then

                    BatchNumberTotalCR = BatchNumberTotalCR + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                    BatchNumberTotalDR = BatchNumberTotalDR + Format(Val((ds.Tables(0).Rows(i).Item("DEBIT_TRANS_AMT")) / 100), "##########.00")
                    ' Total amount of cashout Checks
                    If Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                       Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Then
                        TotalCheckAmount = TotalCheckAmount + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                        TotalCheck = TotalCheck + 1
                    End If
                End If 'if a�adido por modificacion en SQL Query
            Next

            'Valida que el Batch este Contablemente Balanceado (DR=CR)
            If Format(BatchNumberTotalCR, "#######0.00") <> Format(BatchNumberTotalDR, "######0.00") Then
                MsgBox("Batch Not Balanced", vbOKOnly + vbExclamation)
                Exit Sub
            End If

            'Valida que la cantidad entrada por el usuario sea igual a la cantidad de cheques
            If TotalCheck <> Val(txtBatchCheckTotal.Text) Then
                MsgBox("Number of checks must match Batch Check Total : " & TotalCheck, vbOKOnly + vbExclamation)
                txtBatchCheckTotal.Focus()
                Exit Sub
            End If

            'Valida que la cantidad entrada por el usuario sea igual a la cantidad del batch
            If Format(TotalCheckAmount, "########0.00") <> Val(txtBatchTotal.Text) Then
                'MsgBox("Total Amount of the Batch must match BatchNumber Total Amount: " & BatchNumberTotalDR, vbOKOnly + vbExclamation)
                MsgBox("Total Amount of the Batch must match BatchNumber Total Check Amount: " & TotalCheckAmount, vbOKOnly + vbExclamation)
                txtBatchTotal.Focus()
                Exit Sub
            End If

            'Captura Valores del Query en variables globales

            BatchNumber = Trim(txtBatchNbr.Text)

            j = 0

            'Opens File for Writing
            'SourceFile = System.Configuration.ConfigurationSettings.AppSettings("DBConnectionStringInput")
            SourceFile = Application.StartupPath & "\DSERCheck.dat"
            'SourceFile = "\\accdb\dser\DSERCheck.dat"
            oWriter = oFile.CreateText(SourceFile)

            StatusBar1.Text = "Creating GL File"

            'Deletes Records from ProcessLog table
            AccessSQLNonQueryCommand("DELETE * FROM CheckDetail", DBPath)
            AccessSQLNonQueryCommand("DELETE * FROM ProcessLog", DBPath)

            For j = 0 To ds.Tables(0).Rows.Count - 1
                'if a�adido por modificacion en SQL Query
                If (Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "840") And _
                   Trim(ds.Tables(0).Rows(j).Item("REVERSAL_CODE")) = "" Then

                    'Se instancia para usar clase
                    n2w = New CN2W

                    ProcDate = Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 5, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 7, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 1, 4)

                    If Val(ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")) <> 0 Then
                        Account = ds.Tables(0).Rows(j).Item("DEBIT_ACCOUNT")
                    Else
                        Account = ds.Tables(0).Rows(j).Item("CREDIT_ACCOUNT")
                    End If

                    CheckNumber = Format(ds.Tables(0).Rows(j).Item("CHECK_NUMBER"), "00000000")
                    CreditAmt = ds.Tables(0).Rows(j).Item("CREDIT_TRANS_AMT")
                    DebitAmt = ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")
                    SSN = ds.Tables(0).Rows(j).Item("SSN")
                    InsuredName = ds.Tables(0).Rows(j).Item("INSURED")
                    PayorName = ds.Tables(0).Rows(j).Item("PAYOR")
                    PolicyNbr = ds.Tables(0).Rows(j).Item("POLNUM")
                    DBTransCode = ds.Tables(0).Rows(j).Item("DEBIT_CODE")
                    CRTransCode = ds.Tables(0).Rows(j).Item("CREDIT_CODE")
                    EffDate = ds.Tables(0).Rows(j).Item("EFF_DATE")
                    CombinedCheckAmount = ds.Tables(0).Rows(j).Item("COMBINED_CHECK_AMT")

                    TmpAccount = GetGLAcctCode(Account)

                    'Valida que no haya Conseguido en el Account Code Table una cuenta de LifePRO

                    If Val(TmpAccount) = 0 Then
                        MsgBox("Error, GL File Not Created Contact the Information System Deparment")
                        StatusBar1.Text = "Error Finding LifePro Account : " & Account & "on the GL Conversion Table"
                        'Crear log file q escriba la cuenta
                        Exit Sub
                    Else
                        Account = TmpAccount
                    End If

                    Write_GL_File()
                    'A�ade record en tabla ProcessLog
                    SQLString = "INSERT INTO ProcessLog (BatchNumber,Account,DBTransCode,CRTransCode,DbAmount,CrAmount,Insured,SSN,CheckNbr,PolicyNumber) values " & _
                                         "( '" & BatchNumber & "'" & _
                                         ", '" & Account & "'" & _
                                         ", '" & DBTransCode & "'" & _
                                         ", '" & CRTransCode & "'" & _
                                         ", " & DebitAmt & _
                                         ", " & CreditAmt & _
                                         ", '" & InsuredName & "'" & _
                                         ", '" & SSN & "'" & _
                                          ", " & Val(CheckNumber) & _
                                         ", '" & PolicyNbr & "')"

                    AccessSQLNonQueryCommand(SQLString, DBPath)

                    'A�ade record en tabla CheckDetail
                    If BrkCheckNumber <> CheckNumber And (CRTransCode = "92" Or CRTransCode = "93") Then

                        'A�ade record en tabla ProcessLog
                        '"INSERT INTO CheckDetail (policy,checknbr,effdate,amount,textamount,wordamount,payee,insured,address1,address2,address3,address4) values " & _
                        SQLString = "INSERT INTO CheckDetail (policy,checknbr,effdate,amount,textamount,wordamount,transcd,payee,insured) values " & _
                                    "( '" & PolicyNbr & "'" & _
                                    ", " & CheckNumber & _
                                    ", '" & EffDate & "'" & _
                                    ", " & CombinedCheckAmount & _
                                    ", '" & Format(Val(CombinedCheckAmount), "***####.##") & "'" & _
                                    ", '" & n2w.FigureToWord(CombinedCheckAmount) & "'" & _
                                    ", '" & CRTransCode & "'" & _
                                    ", '" & PayorName & "'" & _
                                    ", '" & InsuredName & "')"
                        '", '" & address1 & "'" & _
                        '", '" & address2 & "'" & _
                        '", '" & address3 & "'" & _
                        '", '" & address4 & "')"

                        AccessSQLNonQueryCommand(SQLString, DBPath)
                        BrkCheckNumber = CheckNumber
                    End If

                End If 'if a�adido por modificacion en SQL Query
            Next

            oWriter.Close()
            StatusBar1.Text = "Printing Reports"

            'Crystal Report
            Dim objForm As New frmViewReport
            Dim strReportName As String
            Dim strReportPath As String
            Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
            ''Dim crxDatabaseTable As CRAXDRT.DatabaseTable

            'Reports Path & Location
            strReportName = "ConfirmationReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If


            'Load Report
            rptDocument.Load(strReportPath)


            'To display in the forms title
            objForm.Text = "Confirmation Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface for Batchnumber: " & txtBatchNbr.Text & " '"
            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////

            ''Load Report 2

            'Reports Path & Location
            strReportName = "DetailReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If
            rptDocument.Load(strReportPath)


            'To display in the forms title
            objForm.Text = "Batch Detail Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface Detail for Batchnumber: " & txtBatchNbr.Text & " '"
            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////


            'Copia el archivo a una folder de Backup
            DestinationFile = Application.StartupPath & "\BatchFilesArchive\" & BatchNumber & ".txt"  ' Define target file name.
            FileCopy(SourceFile, DestinationFile)   ' Copy source to target.

            StatusBar1.Text = "Process Completed - File Created"

            Exit Sub
        Catch ex As OleDbException
            MsgBox("There was an OLE DB error '" & (ex.Message))
            End
        Catch exSQL As SqlException
            MsgBox("There was an SQL DB error '" & (exSQL.Message))
            End
        Finally
        End Try

    End Sub

    Private Sub CmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdExit.Click
        Me.Close()
    End Sub
    Private Function WriteCheckRecordFromFile() As Boolean

        'Logica para leer de file de cheques de Lifepro y cargar la informaciona una tabla en access
        Dim lCount As Integer = 0
        Dim Ta
        Dim fsys
        Dim fname As String
        Dim ctrCheck As Long
        Dim ctrCheckLine As Long
        Dim policy As String
        Dim effdate As String
        Dim amount As Double
        Dim textamount As String
        Dim wordamount As String
        Dim payee As String
        Dim insured As String
        Dim address1 As String
        Dim address2 As String
        Dim address3 As String
        Dim address4 As String
        Dim checknbr As Long
        Dim strRec As String
        Dim arrA() As String
        Dim SQLString As String

        Try
            fname = "\\lpapps\lifepro\proden\workarea\rsctyf04.pdm"
            'fname = "\\lpapps\lifepro\proden\workarea\r" & coderid & "f04.pdm"

            'Check if the file exists
            If Not IO.File.Exists(fname) Then
                MsgBox("Unable to locate input file:" & vbCrLf & fname, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                WriteCheckRecordFromFile = False
                Exit Function
            End If

            fsys = CreateObject("scripting.filesystemobject")

            Ta = fsys.OpenTextFile(fname)
            ctrCheck = 0
            ctrCheckLine = 0

            'Deletes Records from ProcessLog table
            AccessSQLNonQueryCommand("DELETE * FROM CheckDetail", DBpath)

            Do While Ta.AtEndOfStream = False
                strRec = Ta.ReadLine
                ctrCheckLine = ctrCheckLine + 1
                If ctrCheckLine = 3 Then
                    effdate = Mid(strRec, 6, 10)
                    amount = Trim(Mid(strRec, 68, 12))
                End If

                If ctrCheckLine = 4 Then
                    policy = Mid(strRec, 6, 12)
                End If

                If ctrCheckLine = 43 Then
                    checknbr = Val(Mid(strRec, 70, 11))
                End If

                If ctrCheckLine = 53 Then
                    insured = Mid(strRec, 23, 50)
                End If

                If ctrCheckLine = 56 Then
                    wordamount = Mid(strRec, 6, 80)
                End If

                If ctrCheckLine = 57 Then
                    textamount = Mid(strRec, 61, 15)
                End If

                If ctrCheckLine = 58 Then
                    payee = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 59 Then
                    address1 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 60 Then
                    address2 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 61 Then
                    address3 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 62 Then
                    address4 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 64 Then
                    ctrCheckLine = 0
                    ctrCheck = ctrCheck + 1
                    Debug.Write(effdate)
                    Debug.Write(policy)
                    Debug.Write(amount)

                    Dim DBpath As String
                    DBpath = Application.StartupPath & "\data\GLACTCodes.mdb"
                    'DBpath = "N:\Programmers\GL Interface System\bin\data\GLACTCodes.mdb"

                    'SQL statement 
                    SQLString = "INSERT INTO CheckDetail (policy,checknbr,effdate,amount,textamount,wordamount,payee,insured,address1,address2,address3,address4) values " & _
                    "( '" & policy & "'" & _
                    ", " & checknbr & _
                    ", '" & effdate & "'" & _
                    ", " & amount & _
                    ", '" & textamount & "'" & _
                    ", '" & wordamount & "'" & _
                    ", '" & payee & "'" & _
                    ", '" & insured & "'" & _
                    ", '" & address1 & "'" & _
                    ", '" & address2 & "'" & _
                    ", '" & address3 & "'" & _
                    ", '" & address4 & "')"
                    AccessSQLNonQueryCommand(SQLString, DBpath)
                End If

            Loop
            'MsgBox("tantos cheques" & ctrCheck)
            WriteCheckRecordFromFile = True
            Exit Function

        Catch dbx As OleDbException
            MsgBox("There was an OLE Db error '" & (dbx.Message) & "' Transaction have been droped")
        Catch ex As Exception
            MsgBox("There was an SQL error '" & (ex.Message) & "' Transaction have been rolled back")
        End Try

    End Function


    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click

        'Llama logica que carga la tabla de donde se alimenta el informe

        If WriteCheckRecordFromFile() = False Then
            Exit Sub
        End If

        'Crystal Report
        Dim objForm As New frmViewReport
        Dim strReportName As String
        Dim strReportPath As String
        Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        ''Dim crxDatabaseTable As CRAXDRT.DatabaseTable

        'Reports Path & Location
        strReportName = "AcctDetail"
        strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

        'Check if the file exists
        If Not IO.File.Exists(strReportPath) Then
            MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
            Exit Sub
        End If


        'Load Report
        rptDocument.Load(strReportPath)


        'To display in the forms title
        objForm.Text = "Check Printing"

        'Database Location (Access -->.mdb) 
        'For Each crxDatabaseTable In rptDocument.Database.Tables
        '    crxDatabaseTable.ConnectionProperties = DBPath
        'Next crxDatabaseTable

        'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
        'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
        'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

        'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


        'Formula Fields 
        'rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface for Batchnumber: " & txtBatchNbr.Text & " '"
        objForm.ShowDialog()

        rptDocument.Dispose()
        rptDocument.Close()
        '/////////////////////////////////////////////////////////////////////////////

    End Sub

End Class