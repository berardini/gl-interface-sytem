Imports System.IO
Imports System.Data.OleDb

Module Module1

    Public rptDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument

    Structure GL_Record
        <VBFixedString(3)> Dim Stat As String
        <VBFixedString(1)> Dim Set_Book_ID As String
        <VBFixedString(7)> Dim Acct_Date As String
        <VBFixedString(3)> Dim Curr_Code As String
        <VBFixedString(7)> Dim Date_Created As String
        <VBFixedString(15)> Dim Created_By As String
        <VBFixedString(1)> Dim Actual_Flag As String
        <VBFixedString(10)> Dim User_Je_Cat_Nam As String
        <VBFixedString(10)> Dim User_Je_Sou_Nam As String
        <VBFixedString(1)> Dim Seg1 As String
        <VBFixedString(1)> Dim Seg2 As String
        <VBFixedString(2)> Dim Seg3 As String
        <VBFixedString(11)> Dim Seg4 As String
        <VBFixedString(2)> Dim Seg5 As String
        <VBFixedString(1)> Dim Seg6 As String
        <VBFixedString(1)> Dim Seg7 As String
        <VBFixedString(3)> Dim Seg8 As String
        <VBFixedString(7)> Dim Seg9 As String
        <VBFixedString(4)> Dim Seg10 As String
        <VBFixedString(11)> Dim Entered_DR As String
        <VBFixedString(11)> Dim Entered_CR As String
        <VBFixedString(7)> Dim Trans_Date As String
        <VBFixedString(3)> Dim Ref1 As String
        <VBFixedString(8)> Dim Ref4 As String
        <VBFixedString(30)> Dim Ref21 As String
        <VBFixedString(12)> Dim Ref22 As String
        <VBFixedString(5)> Dim Period_Nam As String
        <VBFixedString(2)> Dim Grp_ID As String
        ' <VBFixedString(3)> Dim Ref23 As String
        <VBFixedString(2)> Dim CrLf As String
    End Structure
    Public UserID As String
    Public UserName As String
    Public SecurityLevel As String

    Public DBPath As String

    Public OracleRec As GL_Record
    Public GLProcess As String

    Public CompanyBreak As String
    Public DepTotal As Double

    Public oWriter As StreamWriter
    Public oFile As File

    Public CheckTransDate As String
    Public CheckNumber As String
    Public BatchNumber As String
    Public Account As String
    Public CreditAmt As String
    Public DebitAmt As String
    Public CombinedCheckAmount As String
    Public DBTransCode As String
    Public CRTransCode As String
    Public ProcDate As String
    Public SSN As String
    Public InsuredName As String
    Public PayorName As String
    Public PolicyNbr As String
    Public Add1 As String
    Public Add2 As String
    Public Add3 As String
    Public Add4 As String

    Public Sub Write_GL_File()
        Try
            With OracleRec
                .Stat = "NEW"
                .Set_Book_ID = "1"
                .Acct_Date = UCase(Format(CDate(ProcDate), "ddMMMyy"))
                .Curr_Code = "USD"
                .Date_Created = UCase(Format(CDate(ProcDate), "ddMMMyy"))
                .Created_By = "GLInterface    "
                .Actual_Flag = "A"
                .User_Je_Cat_Nam = GLProcess '"DSER      "
                .User_Je_Sou_Nam = GLProcess '"DSER      "
                .Seg1 = "1"
                .Seg2 = "C"
                .Seg3 = "PR"
                .Seg9 = "0000000"
                .Seg10 = "0000"
                .Entered_DR = DebitAmt
                .Entered_CR = CreditAmt
                .Trans_Date = UCase(Format(CDate(ProcDate), "ddMMMyy"))
                .Ref1 = "TRN"
                .Ref4 = CheckNumber
                .Ref21 = InsuredName
                .Ref22 = PolicyNbr ''SSN
                .Period_Nam = UCase(Format(CDate(ProcDate), "MMMyy"))
                .Grp_ID = "10"
                ''.CrLf = Chr(13) & Chr(10)

            End With

            Write_GLDetailRecord()


        Catch

        Finally

        End Try
    End Sub

    Private Sub Write_GLDetailRecord()
        Dim strRec As String


        With OracleRec

            .Seg4 = Mid(Account, 1, 8) + "   "
            'If Len(Account) = 15 Then
            .Seg5 = Mid(Account, 9, 1) + " "
            .Seg6 = Mid(Account, 10, 1)
            .Seg7 = Mid(Account, 11, 1)
            .Seg8 = Mid(Account, 12, 3)
            'Else
            '    .Seg5 = Mid(Account, 9, 2)
            '    .Seg6 = Mid(Account, 10, 1)
            '    .Seg7 = Mid(Account, 11, 1)
            '    .Seg8 = Mid(Account, 12, 3)
            'End If

            If IsNumeric(.Seg5) = False Then
                .Seg5 = "0 "
            End If

            If .Seg6 = " " Then
                .Seg6 = "0"
            End If

            If .Seg7 = " " Then
                .Seg7 = "0"
            End If

            If .Seg8 = "   " Then
                .Seg8 = "000"
            End If
        End With

        strRec = OracleRec.Stat & OracleRec.Set_Book_ID & OracleRec.Acct_Date & OracleRec.Curr_Code & _
          OracleRec.Date_Created & OracleRec.Created_By & OracleRec.Actual_Flag & OracleRec.User_Je_Cat_Nam & _
          OracleRec.User_Je_Sou_Nam & OracleRec.Seg1 & OracleRec.Seg2 & OracleRec.Seg3 & _
          OracleRec.Seg4 & OracleRec.Seg5 & OracleRec.Seg6 & OracleRec.Seg7 & _
          OracleRec.Seg8 & OracleRec.Seg9 & OracleRec.Seg10 & OracleRec.Entered_DR & _
          OracleRec.Entered_CR & OracleRec.Trans_Date & OracleRec.Ref1 & OracleRec.Ref4 & OracleRec.Ref21 & OracleRec.Ref22 & _
          OracleRec.Period_Nam & OracleRec.Grp_ID
        oWriter.WriteLine(strRec) 'Escribe el record al archivo.

    End Sub
    Public Function GetGLAcctCode(ByVal vLPAcct As String) As String

        Dim StrSQL As String

        'Define SQL Connections
        Dim ConnAccess As New OleDb.OleDbConnection

        'Define Data Adapter
        Dim ADP As OleDbDataAdapter

        ' Define Data Set 
        Dim AccessDS As New DataSet

        Try

            ' Assign SQL Connection Strings

            ConnAccess.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data source=" & DBPath


            'Execute Data Adapter

            StrSQL = "Select OracleAcct from GLCodes where LPAcct = '" & vLPAcct & "'"
            ADP = New OleDbDataAdapter(StrSQL, ConnAccess)
            ADP.Fill(AccessDS)

            If AccessDS.Tables(0).Rows.Count <> 0 Then
                GetGLAcctCode = AccessDS.Tables(0).Rows(0).Item("OracleACCT")
            Else
                MsgBox("AcctCode: " & vLPAcct & " Not Found on GL Conversion Table")
                GetGLAcctCode = "000000000000000"
            End If

            ADP = Nothing

        Catch ex As Exception
            MsgBox("Error on Function GetGLAcctCode")
        Catch dbx As OleDbException
            MsgBox("There was an OLE Db error '" & (dbx.Message))
        End Try

    End Function

    Public Function GetCheckTransDate(ByVal vUserid As String, ByVal vCheckNum As String) As String

        Dim StrSQL As String

        'Define SQL Connections
        Dim ConnAccess As New OleDb.OleDbConnection

        'Define Data Adapter
        Dim ADP As OleDbDataAdapter

        ' Define Data Set 
        Dim AccessDS As New DataSet

        Try

            ' Assign SQL Connection Strings

            ConnAccess.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data source=" & DBPath

            'Execute Data Adapter

            StrSQL = "Select CheckTransDate from ProcessLog where Userid = '" & vUserid & "'" & " and Checknbr = '" & vCheckNum & "'"
            ADP = New OleDbDataAdapter(StrSQL, ConnAccess)
            ADP.Fill(AccessDS)

            If AccessDS.Tables(0).Rows.Count <> 0 Then
                GetCheckTransDate = AccessDS.Tables(0).Rows(0).Item("CheckTransDate")
            Else
                MsgBox("Transaction Date not Found for Checknumber: " & vCheckNum & " Not Found on ProcessLog Table")
                GetCheckTransDate = "Error"
            End If

            ADP = Nothing

        Catch ex As Exception
            MsgBox("Error on Function GetCheckTransDate")
        Catch dbx As OleDbException
            MsgBox("There was an OLE Db error '" & (dbx.Message))
        End Try

    End Function

    Public Sub AccessSQLNonQueryCommand(ByVal SqlCommand As String, ByVal dbpath As String)

        'Define SQL COnnections
        Dim AccessConnOutput As New OleDbConnection

        ' Define SQL Command object
        Dim AccessCmdOutput As New OleDbCommand

        Try
            ' Assign SQL Connection Strings
            AccessConnOutput.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data source=" & dbpath

            AccessConnOutput.Open()
            AccessCmdOutput.Connection = AccessConnOutput


            AccessCmdOutput.CommandText = SqlCommand
            AccessCmdOutput.ExecuteNonQuery()


            AccessConnOutput.Close()

            AccessConnOutput = Nothing
            AccessCmdOutput = Nothing

        Catch ex As Exception
            MsgBox("Error on AccessSQLNonQueryCommand SUB")
        Catch dbx As OleDbException
            MsgBox("There was an OLE Db error '" & (dbx.Message))
        End Try

    End Sub

    Public Function InspectString(ByVal WorkString As String) As String

        WorkString = WorkString.Replace("�", "n")
        WorkString = WorkString.Replace("�", "N")
        WorkString = WorkString.Replace("�", "a")
        WorkString = WorkString.Replace("�", "A")
        WorkString = WorkString.Replace("�", "e")
        WorkString = WorkString.Replace("�", "E")
        WorkString = WorkString.Replace("�", "i")
        WorkString = WorkString.Replace("�", "I")
        WorkString = WorkString.Replace("�", "o")
        WorkString = WorkString.Replace("�", "O")
        WorkString = WorkString.Replace("�", "u")
        WorkString = WorkString.Replace("�", "U")
        WorkString = WorkString.Replace("'", "")

        InspectString = WorkString

    End Function

End Module
