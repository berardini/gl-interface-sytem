Imports System.windows.forms.Application
Imports System.Object
Imports System.IO
Imports System.IO.StreamReader
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Public Class frmCheckGLImport

    Inherits System.Windows.Forms.Form

    Dim fName As String
    Dim companybreak As String
    Public DepTotal As Double


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CmdExit As System.Windows.Forms.Button
    Friend WithEvents CmdProcess As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblBatchNbr As System.Windows.Forms.Label
    Friend WithEvents txtBatchNbr As System.Windows.Forms.TextBox
    Friend WithEvents txtBatchTotal As System.Windows.Forms.TextBox
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents lblBatchTotal As System.Windows.Forms.Label
    Friend WithEvents txtBatchCheckTotal As System.Windows.Forms.TextBox
    Friend WithEvents lblBatchCheckTotal As System.Windows.Forms.Label
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CmdProcess = New System.Windows.Forms.Button
        Me.CmdExit = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.txtBatchCheckTotal = New System.Windows.Forms.TextBox
        Me.lblBatchCheckTotal = New System.Windows.Forms.Label
        Me.txtBatchTotal = New System.Windows.Forms.TextBox
        Me.lblBatchTotal = New System.Windows.Forms.Label
        Me.txtBatchNbr = New System.Windows.Forms.TextBox
        Me.lblBatchNbr = New System.Windows.Forms.Label
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdProcess
        '
        Me.CmdProcess.Location = New System.Drawing.Point(112, 144)
        Me.CmdProcess.Name = "CmdProcess"
        Me.CmdProcess.Size = New System.Drawing.Size(88, 24)
        Me.CmdProcess.TabIndex = 3
        Me.CmdProcess.Text = "&Process"
        '
        'CmdExit
        '
        Me.CmdExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdExit.Location = New System.Drawing.Point(216, 144)
        Me.CmdExit.Name = "CmdExit"
        Me.CmdExit.Size = New System.Drawing.Size(88, 24)
        Me.CmdExit.TabIndex = 4
        Me.CmdExit.Text = "E&xit"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdPrint)
        Me.GroupBox1.Controls.Add(Me.txtBatchCheckTotal)
        Me.GroupBox1.Controls.Add(Me.lblBatchCheckTotal)
        Me.GroupBox1.Controls.Add(Me.txtBatchTotal)
        Me.GroupBox1.Controls.Add(Me.lblBatchTotal)
        Me.GroupBox1.Controls.Add(Me.txtBatchNbr)
        Me.GroupBox1.Controls.Add(Me.lblBatchNbr)
        Me.GroupBox1.Controls.Add(Me.CmdProcess)
        Me.GroupBox1.Controls.Add(Me.CmdExit)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(312, 200)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        '
        'cmdPrint
        '
        Me.cmdPrint.Location = New System.Drawing.Point(8, 144)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(88, 24)
        Me.cmdPrint.TabIndex = 32
        Me.cmdPrint.Text = "P&rint"
        '
        'txtBatchCheckTotal
        '
        Me.txtBatchCheckTotal.Location = New System.Drawing.Point(144, 64)
        Me.txtBatchCheckTotal.Name = "txtBatchCheckTotal"
        Me.txtBatchCheckTotal.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchCheckTotal.TabIndex = 1
        '
        'lblBatchCheckTotal
        '
        Me.lblBatchCheckTotal.Location = New System.Drawing.Point(16, 64)
        Me.lblBatchCheckTotal.Name = "lblBatchCheckTotal"
        Me.lblBatchCheckTotal.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchCheckTotal.TabIndex = 31
        Me.lblBatchCheckTotal.Text = "Total Checks"
        '
        'txtBatchTotal
        '
        Me.txtBatchTotal.Location = New System.Drawing.Point(144, 96)
        Me.txtBatchTotal.Name = "txtBatchTotal"
        Me.txtBatchTotal.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchTotal.TabIndex = 2
        '
        'lblBatchTotal
        '
        Me.lblBatchTotal.Location = New System.Drawing.Point(16, 96)
        Me.lblBatchTotal.Name = "lblBatchTotal"
        Me.lblBatchTotal.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchTotal.TabIndex = 29
        Me.lblBatchTotal.Text = "Total Check Amount"
        '
        'txtBatchNbr
        '
        Me.txtBatchNbr.Location = New System.Drawing.Point(144, 32)
        Me.txtBatchNbr.Name = "txtBatchNbr"
        Me.txtBatchNbr.Size = New System.Drawing.Size(144, 20)
        Me.txtBatchNbr.TabIndex = 0
        '
        'lblBatchNbr
        '
        Me.lblBatchNbr.Location = New System.Drawing.Point(16, 32)
        Me.lblBatchNbr.Name = "lblBatchNbr"
        Me.lblBatchNbr.Size = New System.Drawing.Size(120, 24)
        Me.lblBatchNbr.TabIndex = 25
        Me.lblBatchNbr.Text = "Batch Number"
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 251)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(577, 22)
        Me.StatusBar1.TabIndex = 27
        '
        'frmCheckGLImport
        '
        Me.AcceptButton = Me.CmdProcess
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.CmdExit
        Me.ClientSize = New System.Drawing.Size(577, 273)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCheckGLImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LifePro Checks GL Import"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Initialize()
    End Sub

    Private Sub Initialize()
        StatusBar1.Text = ""
        DBPath = Application.StartupPath & "\data\GLACTCodes.mdb"
        GLProcess = "DSER      "
    End Sub

    Private Sub CmdProcess_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdProcess.Click

        Dim ControlNum As String

        Dim SourceFile, DestinationFile As String
        Dim i As Integer
        Dim j As Integer
        Dim CrRptCtr As Integer
        Dim TmpAccount As String
        Dim StrSQL As String
        Dim BatchNumberTrnQty As Long
        Dim BatchNumberTotalCR As Double
        Dim BatchNumberTotalDR As Double
        Dim TotalCheckAmount As Double
        Dim TotalCheck As Long
        Dim SQLString As String
        Dim BrkCheckNumber As String
        Dim n2w As CN2W

        TmpAccount = ""
        BatchNumberTotalCR = 0
        BatchNumberTotalDR = 0
        TotalCheckAmount = 0
        TotalCheck = 0

        If txtBatchNbr.Text = "" Then
            MsgBox("Enter a Batch Number", vbOKOnly + vbExclamation)
            txtBatchNbr.Focus()
            Exit Sub
        End If

        If txtBatchCheckTotal.Text = "" Then
            MsgBox("Enter the Total Check", vbOKOnly + vbExclamation)
            txtBatchCheckTotal.Focus()
            Exit Sub
        End If

        If txtBatchTotal.Text = "" Then
            MsgBox("Enter the Total Check Amount", vbOKOnly + vbExclamation)
            txtBatchTotal.Focus()
            Exit Sub
        End If


        'Define SQL Connections
        Dim connInput As New SqlConnection


        Dim Cmd As New SqlCommand
        Cmd.CommandTimeout = 300
        Cmd.CommandType = CommandType.Text


        'Define Data Adapter
        Dim Adp As SqlDataAdapter

        ' Define Data Set 
        Dim ds As New DataSet


        Try

            ' Assign SQL Connection Strings
            connInput.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("DBConnectionStringInput")

            'VDS 12-03-2009
            'Se a�adio el credit code 841 a peticion CBL 

            StrSQL = "select CHEK.CHECK_NUMBER,CHEK.COMBINED_CHECK_AMT,ACTG.CONTROL_NUMBER,CHEK.CONTROL_NO,ACTG.PAID_TO_DATE_NEW  " & _
                     "into #temp " & _
                     "FROM dbo.PCHEK AS CHEK  " & _
                     "LEFT JOIN PACTG AS ACTG  ON (ACTG.ACTG_KEY6 LIKE CHEK.COMPANY_CODE + CAST(CHEK.VENDOR_ID AS CHAR(12)) + CAST(CHEK.EFFECT_DATE AS CHAR(8)) + '%')    " & _
                     "LEFT JOIN PACTG AS ACTG2 ON (ACTG2.ACTG_KEY5 = ACTG.ACTG_KEY5) AND CHEK.OFFSET_AMOUNT_1 = ACTG2.TRANS_AMOUNT     " & _
                     "WHERE STOCK_NO = 'DSER' " & _
                     "AND CHEK.BATCH_NUMBER = " & "'" & txtBatchNbr.Text & "' " & _
                     "AND NOT (CHECK_STATUS = 'V')   " & _
                     "and CHEK.OFFSET_AMOUNT_1 = ACTG.TRANS_AMOUNT " & _
                     "and ACTG.REVERSAL_CODE = '' " & _
                     "AND ACTG.CREDIT_CODE  NOT IN ('110') " & _
                     "AND ACTG.DEBIT_CODE  NOT IN ('12') " & _
                     "AND ACTG.CREDIT_CODE  IN ('92','93') " & _
                     "ORDER BY CHECK_NUMBER "

            StrSQL = StrSQL + "SELECT CHEK.BATCH_NUMBER, CHEK.CHECK_TRANS_DATE, CHEK.CHECK_NUMBER, CHEK.CHECK_STATUS, " & _
                     "CAST(CHEK.VENDOR_ID AS CHAR(12)) AS POLNUM, CHEK.COMBINED_CHECK_AMT, ACTG.CONTROL_NUMBER, CHEK.CONTROL_NO,CHEK.OFFSET_ACCOUNT_1, " & _
                     "CHEK.EFFECT_DATE AS EFF_DATE,ACTG.EFFECTIVE_DATE AS ACTG_EFF_DATE,ACTG.DEBIT_ACCOUNT,ACTG.CREDIT_ACCOUNT, " & _
                     "ACTG.DEBIT_CODE,ACTG.CREDIT_CODE,ACTG.REVERSAL_CODE, " & _
                     "CASE WHEN (ACTG.CREDIT_CODE IN (413) AND ACTG.DEBIT_CODE IN (13) )  THEN 	'00000000000'  " & _
                     "WHEN NOT  (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840,841))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "ELSE '00000000000' END AS  DEBIT_TRANS_AMT, " & _
                     "CASE WHEN (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840,841))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "WHEN (ACTG.CREDIT_CODE IN (451) AND ACTG.DEBIT_CODE IN (412) )  THEN " & _
                     " 	CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "WHEN (ACTG.CREDIT_CODE IN (413) AND ACTG.DEBIT_CODE IN (13) )  THEN  " & _
                     "	CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11))  " & _
                     "ELSE '00000000000' END AS  CREDIT_TRANS_AMT, " & _
                     "CAST(NAM1.SOC_SEC_NUMBER AS CHAR(12)) AS SSN, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS PAYOR, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS INSURED,ACTG.PAID_TO_DATE_NEW " & _
                     "into #ora " & _
                     "FROM dbo.PCHEK AS CHEK " & _
                     "LEFT JOIN PACTG AS ACTG ON (ACTG.ACTG_KEY6 LIKE CHEK.COMPANY_CODE + CAST(CHEK.VENDOR_ID AS CHAR(12)) + CAST(CHEK.EFFECT_DATE AS CHAR(8)) + '%') " & _
                     "LEFT JOIN PACTG AS ACTG2 ON (ACTG2.ACTG_KEY5 = ACTG.ACTG_KEY5) AND CHEK.COMBINED_CHECK_AMT = ACTG2.TRANS_AMOUNT     " & _
                     "inner join #temp as B on (CHEK.CHECK_NUMBER  = B.CHECK_NUMBER and " & _
                                "ACTG.CONTROL_NUMBER = B.CONTROL_NUMBER) " & _
                     "LEFT JOIN PNAME AS NAM1 ON (CHEK.NAME_ID = NAM1.NAME_ID) " & _
                     "WHERE STOCK_NO = 'DSER' " & _
                     "AND CHEK.BATCH_NUMBER =" & "'" & txtBatchNbr.Text & "' " & _
                     "AND NOT(CHECK_STATUS = 'V') " & _
                     "AND ACTG.CREDIT_CODE  NOT IN ('110') " & _
                     "AND ACTG.DEBIT_CODE  NOT IN ('12') " & _
                     "ORDER BY CHECK_NUMBER "

            StrSQL = StrSQL + "SELECT DISTINCT * FROM #ora "

            StrSQL = StrSQL + "drop table #temp "

            StrSQL = StrSQL + "drop table #ora "


            'Execute Data Adapter

            StatusBar1.Text = "Performing Batch Query"


            Cmd.Connection = connInput
            Cmd.CommandText = StrSQL


            Adp = New SqlDataAdapter

            Adp.SelectCommand = Cmd

            Adp.Fill(ds)

            'Rutina q verifica si hubo transacciones para ese BatchNumber
            If ds.Tables(0).Rows.Count = 0 Then
                MsgBox("DSER Batch Not Found or Batch has no transactions ", vbOKOnly + vbExclamation)
                StatusBar1.Text = ""
                Exit Sub
            End If

            'Cantidad de Cheques transacciones del batch
            BatchNumberTrnQty = ds.Tables(0).Rows.Count
            ''(92, 93,13,1020,560,561,840)
            For i = 0 To ds.Tables(0).Rows.Count - 1

                'VDS 04-15-2009
                'RECHAZAR TRANSACCIONES A LA CUENTA DE UNAPPLIED CASH Y TODAS LAS 
                'TRANSACCIONES QUE ESTEN RELACIONADA A ESE MISMO CONTROL NUMBER

                If Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "12" Then
                    ControlNum = Trim(ds.Tables(0).Rows(i).Item("CONTROL_NUMBER"))
                End If
                '


                'VDS 04-15-2009
                'SE ANADIO Item("CONTROL_NUMBER") <> ControlNum AL IF PARA 
                'RECHAZAR TRANSACCIONES A LA CUENTA DE UNAPPLIED CASH Y TODAS LAS 
                'TRANSACCIONES QUE ESTEN RELACIONADA A ESE MISMO CONTROL NUMBER

                'VDS 12-03-2009
                'Se a�adio el credit code 841 a peticion CBL 

                'if a�adido por modificacion en SQL Query
                If (Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "840" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "413" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "451" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "841") And _
                   Trim(ds.Tables(0).Rows(i).Item("REVERSAL_CODE")) = "" And _
                   Trim(ds.Tables(0).Rows(i).Item("CONTROL_NUMBER") <> ControlNum) Then

                    BatchNumberTotalCR = BatchNumberTotalCR + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                    BatchNumberTotalDR = BatchNumberTotalDR + Format(Val((ds.Tables(0).Rows(i).Item("DEBIT_TRANS_AMT")) / 100), "##########.00")
                    ' Total amount of cashout Checks
                    If Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                       Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Then
                        TotalCheckAmount = TotalCheckAmount + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                        TotalCheck = TotalCheck + 1
                    End If
                End If 'if a�adido por modificacion en SQL Query
            Next 'end Loop

            'Valida que el Batch este Contablemente Balanceado (DR=CR)
            If Format(BatchNumberTotalCR, "#######0.00") <> Format(BatchNumberTotalDR, "######0.00") Then
                MsgBox("Batch Not Balanced", vbOKOnly + vbExclamation)
                Exit Sub
            End If

            'Valida que la cantidad entrada por el usuario sea igual a la cantidad de cheques
            If TotalCheck <> Val(txtBatchCheckTotal.Text) Then
                MsgBox("Number of checks must match Batch Check Total : " & TotalCheck, vbOKOnly + vbExclamation)
                txtBatchCheckTotal.Focus()
                Exit Sub
            End If

            'Valida que la cantidad entrada por el usuario sea igual a la cantidad del batch
            If Format(TotalCheckAmount, "########0.00") <> Val(txtBatchTotal.Text) Then
                MsgBox("Total Amount of the Batch must match Batch Number Total Check Amount: " & TotalCheckAmount, vbOKOnly + vbExclamation)
                txtBatchTotal.Focus()
                Exit Sub
            End If

            'Captura Valores del Query en variables globales

            BatchNumber = Trim(txtBatchNbr.Text)

            j = 0

            'Opens File for Writing
            SourceFile = Application.StartupPath & "\DSERCheck.dat"

            'Check if the file exists for append
            If IO.File.Exists(SourceFile) Then
                Select Case MsgBox("File Already Exists. Do you want the information generated to be append to this existing file?", MsgBoxStyle.YesNoCancel, "File Found")
                    Case MsgBoxResult.Yes
                        oWriter = oFile.AppendText(SourceFile)
                    Case MsgBoxResult.No
                        oWriter = oFile.CreateText(SourceFile)
                    Case MsgBoxResult.Cancel
                        StatusBar1.Text = "Process Canceled by user"
                        Exit Sub
                End Select
            Else
                oWriter = oFile.CreateText(SourceFile)
            End If

            'oWriter = oFile.AppendText(SourceFile)

            StatusBar1.Text = "Creating GL File"

            'Deletes Records from ProcessLog table
            AccessSQLNonQueryCommand("DELETE * FROM CheckDetail", DBPath)
            AccessSQLNonQueryCommand("DELETE * FROM ProcessLog WHERE UserID = '" & UserID & "'", DBPath)

            For j = 0 To ds.Tables(0).Rows.Count - 1

                'VDS 04-15-2009
                'RECHAZAR TRANSACCIONES A LA CUENTA DE UNAPPLIED CASH Y TODAS LAS 
                'TRANSACCIONES QUE ESTEN RELACIONADA A ESE MISMO CONTROL NUMBER

                If Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "12" Then
                    ControlNum = Trim(ds.Tables(0).Rows(j).Item("CONTROL_NUMBER"))
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                'VDS 04-15-2009
                'SE ANADIO Item("CONTROL_NUMBER") <> ControlNum AL IF PARA 
                'RECHAZAR TRANSACCIONES A LA CUENTA DE UNAPPLIED CASH Y TODAS LAS 
                'TRANSACCIONES QUE ESTEN RELACIONADA A ESE MISMO CONTROL NUMBER

                'VDS 12-03-2009
                'Se a�adio el credit code 841 a peticion CBL 

                'if a�adido por modificacion en SQL Query
                If (Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "840" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "413" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "451" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "841") And _
                   Trim(ds.Tables(0).Rows(j).Item("CONTROL_NUMBER") <> ControlNum) And _
                   Trim(ds.Tables(0).Rows(j).Item("REVERSAL_CODE")) = "" Then

                    'Se instancia para usar clase
                    'n2w = New CN2W

                    ProcDate = Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 5, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 7, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("CHECK_TRANS_DATE"), 1, 4)

                    If Val(ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")) <> 0 Then
                        Account = ds.Tables(0).Rows(j).Item("DEBIT_ACCOUNT")
                    Else
                        Account = ds.Tables(0).Rows(j).Item("CREDIT_ACCOUNT")
                    End If

                    CheckNumber = Format(ds.Tables(0).Rows(j).Item("CHECK_NUMBER"), "00000000")
                    CreditAmt = ds.Tables(0).Rows(j).Item("CREDIT_TRANS_AMT")
                    DebitAmt = ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")
                    SSN = ds.Tables(0).Rows(j).Item("SSN")
                    InsuredName = InspectString(ds.Tables(0).Rows(j).Item("INSURED"))
                    PayorName = ds.Tables(0).Rows(j).Item("PAYOR")
                    PolicyNbr = ds.Tables(0).Rows(j).Item("POLNUM")
                    DBTransCode = ds.Tables(0).Rows(j).Item("DEBIT_CODE")
                    CRTransCode = ds.Tables(0).Rows(j).Item("CREDIT_CODE")
                    CheckTransDate = ProcDate
                    CombinedCheckAmount = ds.Tables(0).Rows(j).Item("COMBINED_CHECK_AMT")

                    TmpAccount = GetGLAcctCode(Account)

                    'Valida que no haya Conseguido en el Account Code Table una cuenta de LifePRO

                    If Val(TmpAccount) = 0 Then
                        MsgBox("Error Finding LifePro Account : " & Account & " on the GL Conversion Table for Policy Number " & PolicyNbr & " Checknumber: " & CheckNumber)
                        StatusBar1.Text = "Error on Policy Number " & PolicyNbr & " Checknumber: " & CheckNumber
                        MsgBox("Error, GL File Not Created Contact the Information System Deparment")
                        'Crear log file q escriba la cuenta
                        Exit Sub
                    Else
                        Account = TmpAccount
                    End If

                    Write_GL_File()
                    'A�ade record en tabla ProcessLog
                    SQLString = "INSERT INTO ProcessLog (UserID,BatchNumber,Account,DBTransCode,CRTransCode,DbAmount,CrAmount,Insured,SSN,CheckNbr,CheckTransDate,PolicyNumber) values " & _
                                         "( '" & UserID & "'" & _
                                         ", '" & BatchNumber & "'" & _
                                         ", '" & Account & "'" & _
                                         ", '" & DBTransCode & "'" & _
                                         ", '" & CRTransCode & "'" & _
                                         ", " & DebitAmt & _
                                         ", " & CreditAmt & _
                                         ", '" & InspectString(InsuredName) & "'" & _
                                         ", '" & SSN & "'" & _
                                         ", " & Val(CheckNumber) & _
                                         ", '" & CheckTransDate & "'" & _
                                         ", '" & PolicyNbr & "')"

                    AccessSQLNonQueryCommand(SQLString, DBPath)

                    ''A�ade record en tabla CheckDetail
                    'If BrkCheckNumber <> CheckNumber And (CRTransCode = "92" Or CRTransCode = "93") Then

                    '    'A�ade record en tabla ProcessLog
                    '    '"INSERT INTO CheckDetail (policy,checknbr,effdate,amount,textamount,wordamount,payee,insured,address1,address2,address3,address4) values " & _
                    '    SQLString = "INSERT INTO CheckDetail (policy,checknbr,effdate,amount,textamount,wordamount,transcd,payee,insured) values " & _
                    '                "( '" & PolicyNbr & "'" & _
                    '                ", " & CheckNumber & _
                    '                ", '" & EffDate & "'" & _
                    '                ", " & CombinedCheckAmount & _
                    '                ", '" & Format(Val(CombinedCheckAmount), "***####.##") & "'" & _
                    '                ", '" & n2w.FigureToWord(CombinedCheckAmount) & "'" & _
                    '                ", '" & CRTransCode & "'" & _
                    '                ", '" & PayorName & "'" & _
                    '                ", '" & InsuredName & "')"
                    '    '", '" & address1 & "'" & _
                    '    '", '" & address2 & "'" & _
                    '    '", '" & address3 & "'" & _
                    '    '", '" & address4 & "')"

                    '    AccessSQLNonQueryCommand(SQLString, DBPath)
                    '    BrkCheckNumber = CheckNumber
                    'End If

                End If 'if a�adido por modificacion en SQL Query
            Next

            oWriter.Close()
            StatusBar1.Text = "Printing Reports"

            'Crystal Report
            Dim objForm As New frmViewReport
            Dim strReportName As String
            Dim strReportPath As String
            Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo

            'Reports Path & Location
            strReportName = "ConfirmationReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If

            'Load Report
            rptDocument.Load(strReportPath)

            'To display in the forms title
            objForm.Text = "Confirmation Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface for Batch Number: " & txtBatchNbr.Text & " '"

            rptDocument.RecordSelectionFormula = "{ProcessLog.UserID} = " & "'" & UserID & "'"

            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////

            ''Load Report 2

            'Reports Path & Location
            strReportName = "DetailReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If
            rptDocument.Load(strReportPath)


            'To display in the forms title
            'objForm.Text = "Batch Detail Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface Detail for Batch Number: " & txtBatchNbr.Text & " '"
            rptDocument.RecordSelectionFormula = "{ProcessLog.UserID} = " & "'" & UserID & "'"
            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////


            'Copia el archivo a un folder de Backup
            DestinationFile = Application.StartupPath & "\BatchFilesArchive\" & BatchNumber & ".txt"  ' Define target file name.
            FileCopy(SourceFile, DestinationFile)   ' Copy source to target.

            StatusBar1.Text = "Process Completed - GL File Created"

            Exit Sub
        Catch ex As Exception
            MsgBox("There was an error on CmdProcess_click SUB '" & (ex.Message))
            End
        Catch OLEDBex As OleDbException
            MsgBox("There was an OLE DB error '" & (OLEDBex.Message))
            End
        Catch exSQL As SqlException
            MsgBox("There was an SQL DB error '" & (exSQL.Message))
            End
        End Try

    End Sub

    Private Sub CmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdExit.Click
        Me.Close()
    End Sub
    Private Function WriteCheckRecordFromFile() As Boolean

        'Logica para leer de file de cheques de Lifepro y cargar la informaciona una tabla en access
        Dim lCount As Integer = 0
        Dim Ta As Object
        Dim fsys As Object
        Dim fname As String
        Dim ctrCheck As Long
        Dim ctrCheckLine As Long
        Dim policy As String
        Dim TmpTransdate As String
        Dim transdate As String
        Dim amount As Double
        Dim textamount As String
        Dim wordamount As String
        Dim payee As String
        Dim insured As String
        Dim address1 As String
        Dim address2 As String
        Dim address3 As String
        Dim address4 As String
        Dim checknbr As Long
        Dim strRec As String
        Dim arrA() As String
        Dim SQLString As String


        TmpTransdate = ""

        Try
            'Path del archivo de input de los cheques de LifePro
            fname = System.Configuration.ConfigurationManager.AppSettings("LPPrintCheckFile") & "r" & UserID & "f04.pdm"
            'fname = "\\lpapps\lifepro\prod\workarea\r" & UserID & "f04.pdm"
            ' fname = "\\sqlc-02\dev_area\Apps32\LifePro-Dev\Programmers\GL Interface System\bin\data\r" & UserID & "f04.pdm"

            'Check if the file exists
            If Not IO.File.Exists(fname) Then
                MsgBox("Unable to locate input file:" & vbCrLf & fname, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                WriteCheckRecordFromFile = False
                Exit Function
            End If


            Dim myStream As System.IO.StreamReader = New System.IO.StreamReader(fname)

            'fsys = CreateObject("scripting.filesystemobject")

            'Ta = fsys.OpenTextFile(fname)
            ctrCheck = 0
            ctrCheckLine = 0

            'Deletes Records from ProcessLog table
            AccessSQLNonQueryCommand("DELETE * FROM CheckDetail", DBPath)

            Do
                ' replaced  Do While Ta.AtEndOfStream = False
                ' replaced  strRec = Ta.ReadLine
                strRec = myStream.ReadLine()

                If strRec Is Nothing Then
                    Exit Do
                End If

                ctrCheckLine = ctrCheckLine + 1
                If ctrCheckLine = 3 Then
                    transdate = Mid(strRec, 6, 10)
                    amount = Trim(Mid(strRec, 68, 12))
                End If

                If ctrCheckLine = 4 Then
                    policy = Mid(strRec, 6, 12)
                End If

                If ctrCheckLine = 43 Then
                    checknbr = Val(Mid(strRec, 70, 11))
                End If

                If ctrCheckLine = 58 Then
                    insured = Mid(strRec, 23, 50)
                End If

                If ctrCheckLine = 56 Then
                    wordamount = Mid(strRec, 6, 80)
                End If

                If ctrCheckLine = 57 Then
                    textamount = Mid(strRec, 61, 15)
                End If

                If ctrCheckLine = 58 Then
                    payee = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 59 Then
                    address1 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 60 Then
                    address2 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 61 Then
                    address3 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 62 Then
                    address4 = Mid(strRec, 8, 50)
                End If

                If ctrCheckLine = 64 Then

                    'Logica para sustituir la fecha de Effectividad que trae el cheque impreso desde LifePro por la
                    'Fecha de Transaccion del cheque.

                    If textamount <> "$$,$$$,$$$.$$" Then
                        TmpTransdate = GetCheckTransDate(UserID, checknbr)
                    End If

                    If (TmpTransdate = "Error") Then
                        MsgBox("Transaction Date not Found for Checknumber: " & checknbr & " on ProcessLog Table")
                        StatusBar1.Text = "Transaction Date not Found for Checknumber: " & checknbr & " on ProcessLog Table"
                        transdate = "00/00/0000"
                    Else
                        transdate = TmpTransdate
                    End If


                    ctrCheckLine = 0
                    ctrCheck = ctrCheck + 1


                    Debug.Write(transdate)
                    Debug.Write(policy)
                    Debug.Write(amount)



                    'SQL statement 
                    If textamount <> "$$,$$$,$$$.$$" Then

                        SQLString = "INSERT INTO CheckDetail (userid,policy,checknbr,transdate,amount,textamount,wordamount,payee,insured,address1,address2,address3,address4) values " & _
                        "( '" & UserID & "'" & _
                        ", '" & policy & "'" & _
                        ", " & checknbr & _
                        ", '" & transdate & "'" & _
                        ", " & amount & _
                        ", '" & textamount & "'" & _
                        ", '" & wordamount & "'" & _
                        ", '" & InspectString(payee) & "'" & _
                        ", '" & InspectString(insured) & "'" & _
                        ", '" & address1 & "'" & _
                        ", '" & address2 & "'" & _
                        ", '" & address3 & "'" & _
                        ", '" & address4 & "')"
                        AccessSQLNonQueryCommand(SQLString, DBPath)
                    End If
                End If

            Loop
            myStream.Close()
            WriteCheckRecordFromFile = True
            Exit Function

        Catch OLEDBex As OleDbException
            MsgBox("There was an OLE Db error '" & (OLEDBex.Message))
        Catch ex As Exception
            MsgBox("There was an error on WriteCheckRecordFromFile SUB '" & (ex.Message))
        End Try

    End Function


    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click

        'Llama logica que carga la tabla de donde se alimenta el informe

        If WriteCheckRecordFromFile() = False Then
            StatusBar1.Text = "Error - In LP Print File"
            Exit Sub
        End If

        'Crystal Report
        'Dim objForm As New frmCrystalViewerNew
        Dim strReportName As String
        Dim strReportPath As String
        Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim mySubReportObject As CrystalDecisions.CrystalReports.Engine.SubreportObject

        'Sub report document of crystal report.
        'Dim mySubRepDoc As New CrystalDecisions.CrystalReports.Engine.ReportDocument

        Try


            'Reports Path & Location
            strReportName = "AcctDetail"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If


            'Load Report
            rptDocument.Load(strReportPath)

            StatusBar1.Text = "Printing..."


            'mySubRepDoc = rptDocument.OpenSubreport("AcctDetail")
            'mySubRepDoc.RecordSelectionFormula = "{ProcessLog.UserID} = " & "'" & UserID & "' and " & _
            '                                     "{ProcessLog.PolicyNumber} = {?Pm-CheckDetail.policy} and " & _
            '                                     "{ProcessLog.CheckNbr} = {?Pm-CheckDetail.checknbr}"
            rptDocument.RecordSelectionFormula = "{CheckDetail.UserID} = " & "'" & UserID & "'"
            'rptDocument.PrintToPrinter(1, False, 1, 1000)

                'Database Location (Access -->.mdb) 
                'For Each crxDatabaseTable In rptDocument.Database.Tables
                '    crxDatabaseTable.ConnectionProperties = DBPath
                'Next crxDatabaseTable

                'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
                'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
                'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


                'Formula Fields 
                'rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface for Batchnumber: " & txtBatchNbr.Text & " '"
            Dim objForm As New frmCrystalViewerNew(rptDocument, True, True, True, True, "")

            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
                '/////////////////////////////////////////////////////////////////////////////
            StatusBar1.Text = "Printing Process Completed"

        Catch ex As Exception
            MsgBox("There was an error Printing - " & (ex.Message) & "' ")
            End
        End Try

    End Sub
End Class