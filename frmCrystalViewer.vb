
Public Class frmViewReport
    Inherits System.Windows.Forms.Form

    '**********************************************************************************
    'Instrucciones:    
    '1. Copiar variable p�blica en un m�dulo.
    '
    '//////////////////////////////////////////////////////////////////////////////
    'Public rptDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    '//////////////////////////////////////////////////////////////////////////////
    '
    '2. Copiar el siguiente c�digo en el proceso que va a ejecutar el reporte y
    '   configurarlo seg�n las necesidades requeridas por el programa.
    '
    '/////////////////////////////////////////////////////////////////////////////
    'Dim objForm As New frmViewReport
    'Dim strReportName As String
    'Dim strReportPath As String
    'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
    'Dim i As Integer

    ''Reports Path & Location
    '    strReportName = "Copy of Annuities Subsidiary"
    '    strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

    ''Check if the file exists
    '    If Not IO.File.Exists(strReportPath) Then
    '        MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
    '        Exit Sub
    '    End If

    ''Load Report
    '    rptDocument.Load(strReportPath)

    ''*************************************************************************
    ''Connection for SQL
    ''*************************************************************************
    ''Set the connection information to ConInfo object so that we can apply the 
    '' connection information on each table in the reporteport
    ''ConInfo.ConnectionInfo.UserID = "gadev"
    ''ConInfo.ConnectionInfo.Password = "Developer1*"
    ''ConInfo.ConnectionInfo.ServerName = "LPSQL"
    ''ConInfo.ConnectionInfo.DatabaseName = "PROD"

    ''For i = 0 To rptDocument.Database.Tables.Count - 1
    ''    rptDocument.Database.Tables(i).ApplyLogOnInfo(ConInfo)
    ''Next
    ''*************************************************************************

    ''Database Location (Access -->.mdb) 
    'rptDocument.Database.Tables(0).Location = Application.StartupPath & "\data\MDB1.mdb"

    ''Parameters
    '    rptDocument.SetParameterValue(0, "Parametro 0")
    '    rptDocument.SetParameterValue(1, "Parametro 1")

    ''SelectionFormula
    '    rptDocument.DataDefinition.RecordSelectionFormula = "{Fund_Balances_FDAN_Series.FirstName} = 'MARIA'"

    ''To display in the forms title
    '    objForm.Text = "Change Title!!!"

    '    objForm.ShowDialog()    

    '    rptDocument.Dispose()
    '    rptDocument.Close()
    '/////////////////////////////////////////////////////////////////////////////

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rptViewer.Location = New System.Drawing.Point(0, 0)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.SelectionFormula = ""
        Me.rptViewer.Size = New System.Drawing.Size(664, 462)
        Me.rptViewer.TabIndex = 0
        Me.rptViewer.ViewTimeSelectionFormula = ""
        '
        'frmViewReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 462)
        Me.Controls.Add(Me.rptViewer)
        Me.Name = "frmViewReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form 2"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub frmViewReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptViewer.ShowRefreshButton = False
        rptViewer.ShowCloseButton = False
        rptViewer.ShowGroupTreeButton = False
        rptViewer.ReportSource = rptDocument
        rptViewer.Show()
    End Sub

    Private Sub rptViewer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rptViewer.Load

    End Sub
End Class



