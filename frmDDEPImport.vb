Imports System.windows.forms.Application
Imports System.Object
Imports System.IO
Imports System.IO.StreamReader
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class frmDDEPImport
    Inherits System.Windows.Forms.Form


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CmdExit As System.Windows.Forms.Button
    Friend WithEvents CmdProcess As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblBatchNbr As System.Windows.Forms.Label
    Friend WithEvents txtBatchNbr As System.Windows.Forms.TextBox
    Friend WithEvents txtBatchTotal As System.Windows.Forms.TextBox
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents lblBatchTotal As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CmdProcess = New System.Windows.Forms.Button
        Me.CmdExit = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtBatchTotal = New System.Windows.Forms.TextBox
        Me.lblBatchTotal = New System.Windows.Forms.Label
        Me.txtBatchNbr = New System.Windows.Forms.TextBox
        Me.lblBatchNbr = New System.Windows.Forms.Label
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdProcess
        '
        Me.CmdProcess.Location = New System.Drawing.Point(67, 128)
        Me.CmdProcess.Name = "CmdProcess"
        Me.CmdProcess.Size = New System.Drawing.Size(123, 30)
        Me.CmdProcess.TabIndex = 3
        Me.CmdProcess.Text = "&Process"
        '
        'CmdExit
        '
        Me.CmdExit.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdExit.Location = New System.Drawing.Point(269, 128)
        Me.CmdExit.Name = "CmdExit"
        Me.CmdExit.Size = New System.Drawing.Size(123, 30)
        Me.CmdExit.TabIndex = 4
        Me.CmdExit.Text = "E&xit"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtBatchTotal)
        Me.GroupBox1.Controls.Add(Me.lblBatchTotal)
        Me.GroupBox1.Controls.Add(Me.txtBatchNbr)
        Me.GroupBox1.Controls.Add(Me.lblBatchNbr)
        Me.GroupBox1.Controls.Add(Me.CmdProcess)
        Me.GroupBox1.Controls.Add(Me.CmdExit)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(437, 187)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        '
        'txtBatchTotal
        '
        Me.txtBatchTotal.Location = New System.Drawing.Point(202, 79)
        Me.txtBatchTotal.Name = "txtBatchTotal"
        Me.txtBatchTotal.Size = New System.Drawing.Size(201, 23)
        Me.txtBatchTotal.TabIndex = 2
        '
        'lblBatchTotal
        '
        Me.lblBatchTotal.Location = New System.Drawing.Point(22, 69)
        Me.lblBatchTotal.Name = "lblBatchTotal"
        Me.lblBatchTotal.Size = New System.Drawing.Size(168, 29)
        Me.lblBatchTotal.TabIndex = 29
        Me.lblBatchTotal.Text = "Total Batch Amount"
        '
        'txtBatchNbr
        '
        Me.txtBatchNbr.Location = New System.Drawing.Point(202, 39)
        Me.txtBatchNbr.Name = "txtBatchNbr"
        Me.txtBatchNbr.Size = New System.Drawing.Size(201, 23)
        Me.txtBatchNbr.TabIndex = 0
        '
        'lblBatchNbr
        '
        Me.lblBatchNbr.Location = New System.Drawing.Point(22, 39)
        Me.lblBatchNbr.Name = "lblBatchNbr"
        Me.lblBatchNbr.Size = New System.Drawing.Size(168, 30)
        Me.lblBatchNbr.TabIndex = 25
        Me.lblBatchNbr.Text = "Batch Number"
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 185)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(489, 27)
        Me.StatusBar1.TabIndex = 27
        '
        'frmDDEPImport
        '
        Me.AcceptButton = Me.CmdProcess
        Me.AutoScaleBaseSize = New System.Drawing.Size(7, 16)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.CmdExit
        Me.ClientSize = New System.Drawing.Size(489, 212)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDDEPImport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LifePro Direct Deposits GL Import"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub frmDDEPImport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Initialize()
    End Sub

    Private Sub Initialize()
        StatusBar1.Text = ""
        GLProcess = "DDEP      "
        DBPath = Application.StartupPath & "\data\GLACTCodes.mdb"
    End Sub
    Private Sub CmdProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdProcess.Click

        Dim SourceFile, DestinationFile As String
        Dim i As Integer
        Dim j As Integer
        Dim CrRptCtr As Integer
        Dim TmpAccount As String
        Dim StrSQL As String
        Dim BatchNumberTrnQty As Long
        Dim BatchNumberTotalCR As Double
        Dim BatchNumberTotalDR As Double
        Dim TotalCheckAmount As Double
        Dim TotalCheck As Long
        Dim SurrenderCharges As Double
        Dim SQLString As String
        Dim BrkCheckNumber As String
        Dim appendanswer As String

        TmpAccount = ""
        BatchNumberTotalCR = 0
        BatchNumberTotalDR = 0
        SurrenderCharges = 0
        TotalCheckAmount = 0
        TotalCheck = 0

        If txtBatchNbr.Text = "" Then
            MsgBox("Enter a BatchNumber", vbOKOnly + vbExclamation)
            txtBatchNbr.Focus()
            Exit Sub
        End If


        If txtBatchTotal.Text = "" Then
            MsgBox("Enter the Total Batch Amount", vbOKOnly + vbExclamation)
            txtBatchTotal.Focus()
            Exit Sub
        End If


        'Define SQL Connections
        Dim connInput As New SqlConnection

        Dim Cmd As New SqlCommand
        Cmd.CommandTimeout = 300
        Cmd.CommandType = CommandType.Text


        'Define Data Adapter
        Dim Adp As SqlDataAdapter

        ' Define Data Set 
        Dim ds As New DataSet


        Try
            ' Assign SQL Connection Strings
            connInput.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("DBConnectionStringInput")

            'VDS 12-03-2009
            'Se a�adio el credit code 841 a peticion CBL 

            StrSQL = "SELECT CHEK.BATCH_NUMBER, CHEK.CHECK_TRANS_DATE, CHEK.CHECK_NUMBER, CHEK.CHECK_STATUS, " & _
                     "CAST(CHEK.VENDOR_ID AS CHAR(12)) AS POLNUM, CHEK.COMBINED_CHECK_AMT, CHEK.ACTG_CONTROL_NUMBR, CHEK.CONTROL_NO,CHEK.OFFSET_ACCOUNT_1, " & _
                     "CHEK.EFFECT_DATE AS EFF_DATE,ACTG.EFFECTIVE_DATE AS ACTG_EFF_DATE,ACTG.DEBIT_ACCOUNT,ACTG.CREDIT_ACCOUNT, " & _
                     "ACTG.DEBIT_CODE,ACTG.CREDIT_CODE,ACTG.REVERSAL_CODE, " & _
                     "CASE WHEN NOT  (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840,841))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "ELSE '00000000000' END AS  DEBIT_TRANS_AMT, " & _
                     "CASE WHEN (ACTG.CREDIT_CODE IN (92, 93,1020,560,561,840,841))  THEN " & _
                     "CAST((RIGHT('00000000000'+ RTRIM(CAST(CAST((ACTG.TRANS_AMOUNT * 100) AS int)as char(11))), 11)) AS char(11)) " & _
                     "ELSE '00000000000' END AS  CREDIT_TRANS_AMT, " & _
                     "CAST(NAM1.SOC_SEC_NUMBER AS CHAR(12)) AS SSN, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS PAYOR, " & _
                     "CAST(RTRIM(NAM1.INDIVIDUAL_FIRST) + ' ' + RTRIM(NAM1.INDIVIDUAL_LAST) AS CHAR(30)) AS INSURED " & _
                     "FROM dbo.PCHEK AS CHEK " & _
                     "LEFT JOIN PACTG AS ACTG ON (ACTG.ACTG_KEY6 LIKE CHEK.COMPANY_CODE + CAST(CHEK.VENDOR_ID AS CHAR(12)) + CAST(CHEK.EFFECT_DATE AS CHAR(8)) + '%') " & _
                     "LEFT JOIN PNAME AS NAM1 ON (CHEK.NAME_ID = NAM1.NAME_ID) " & _
                     "WHERE STOCK_NO = 'DDEP' " & _
                     "AND CHEK.BATCH_NUMBER =" & "'" & txtBatchNbr.Text & "' " & _
                     "AND CHEK.CHECK_STATUS = 'C' AND CHEK.CHECK_TYPE = 'P' " & _
                     "AND CHEK.CHECK_NUMBER NOT IN (68250,68251)   " & _
                     "AND ACTG.REVERSAL_CODE = ''  " & _
                     "ORDER BY CHECK_NUMBER"

            'Execute Data Adapter

            StatusBar1.Text = "Performing Batch Query"

            Cmd.Connection = connInput
            Cmd.CommandText = StrSQL


            Adp = New SqlDataAdapter

            Adp.SelectCommand = Cmd

            Adp.Fill(ds)

            'Rutina q verifica si hubo transacciones para ese BatchNumber
            If ds.Tables(0).Rows.Count = 0 Then
                MsgBox("DDEP Batch Not Found or Batchnumber has no transactions", vbOKOnly + vbExclamation)
                StatusBar1.Text = ""
                Exit Sub
            End If

            'Cantidad de DDEP transacciones del batch
            BatchNumberTrnQty = ds.Tables(0).Rows.Count
            For i = 0 To ds.Tables(0).Rows.Count - 1
                'if a�adido por modificacion en SQL Query

                'VDS 12-03-2009
                'Se a�adio el credit code 841 a peticion CBL 

                If (Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "840" Or _
                   Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "841") And _
                   Trim(ds.Tables(0).Rows(i).Item("REVERSAL_CODE")) = "" Then
                    BatchNumberTotalCR = BatchNumberTotalCR + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                    BatchNumberTotalDR = BatchNumberTotalDR + Format(Val((ds.Tables(0).Rows(i).Item("DEBIT_TRANS_AMT")) / 100), "##########.00")

                    ' Total amount of cash out(Direct Deposit Amount) 
                    If Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "93" Or _
                       Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "92" Then
                        TotalCheckAmount = TotalCheckAmount + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                    End If

                    'Total amount of Surrender Charges
                    'VDS 12-03-2009
                    'Se a�adio el credit code 841 a peticion CBL 
                    If Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "1020" Or _
                       Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "840" Or _
                       Trim(ds.Tables(0).Rows(i).Item("CREDIT_CODE")) = "841" Then
                        SurrenderCharges = SurrenderCharges + Format(Val((ds.Tables(0).Rows(i).Item("CREDIT_TRANS_AMT")) / 100), "##########.00")
                    End If

                End If 'if a�adido por modificacion en SQL Query
            Next

            'Valida que el Batch este Contablemente Balanceado (DR=CR)
            If Format(BatchNumberTotalCR, "#######0.00") <> Format(BatchNumberTotalDR, "######0.00") Then
                MsgBox("Batch Not Balanced", vbOKOnly + vbExclamation)
                StatusBar1.Text = ""
                Exit Sub
            End If


            'Valida que la cantidad entrada por el usuario sea igual a la cantidad del batch
            If Format(TotalCheckAmount, "########0.00") <> Val(txtBatchTotal.Text) Then
                'MsgBox("Total Amount of the Batch must match BatchNumber Total Amount: " & BatchNumberTotalDR, vbOKOnly + vbExclamation)
                MsgBox("Total Amount of the Batch must match BatchNumber Total Batch Amount: " & TotalCheckAmount, vbOKOnly + vbExclamation)
                txtBatchTotal.Focus()
                StatusBar1.Text = ""
                Exit Sub
            End If

            'Captura Valores del Query en variables globales

            BatchNumber = Trim(txtBatchNbr.Text)

            j = 0

            'Opens File for Writing
            SourceFile = Application.StartupPath & "\DDEPCheck.dat"


            'Check if the file exists for append
            If IO.File.Exists(SourceFile) Then
                Select Case MsgBox("File Already Exists. Do you want the information generated to be append to this existing file?", MsgBoxStyle.YesNoCancel, "File Found")
                    Case MsgBoxResult.Yes
                        oWriter = oFile.AppendText(SourceFile)
                    Case MsgBoxResult.No
                        oWriter = oFile.CreateText(SourceFile)
                    Case MsgBoxResult.Cancel
                        StatusBar1.Text = "Process Canceled by user"
                        Exit Sub
                End Select
            Else
                oWriter = oFile.CreateText(SourceFile)
            End If

            'oWriter = oFile.AppendText(SourceFile)

            StatusBar1.Text = "Creating GL File"

            'Deletes Records from ProcessLog table
            AccessSQLNonQueryCommand("DELETE * FROM ProcessLog WHERE UserID = '" & UserID & "'", DBPath)

            For j = 0 To ds.Tables(0).Rows.Count - 1

                'if a�adido por modificacion en SQL Query

                'VDS 12-03-2009
                'Se a�adio el credit code 841 a peticion CBL 
                If (Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "92" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "93" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "13" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "1020" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "560" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "561" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "840" Or _
                   Trim(ds.Tables(0).Rows(j).Item("CREDIT_CODE")) = "841") And _
                   Trim(ds.Tables(0).Rows(j).Item("REVERSAL_CODE")) = "" Then

                    ProcDate = Mid(ds.Tables(0).Rows(j).Item("EFF_DATE"), 5, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("EFF_DATE"), 7, 2) & "/" & Mid(ds.Tables(0).Rows(j).Item("EFF_DATE"), 1, 4)

                    If Val(ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")) <> 0 Then
                        Account = ds.Tables(0).Rows(j).Item("DEBIT_ACCOUNT")
                    Else
                        Account = ds.Tables(0).Rows(j).Item("CREDIT_ACCOUNT")
                    End If

                    CheckNumber = Format(ds.Tables(0).Rows(j).Item("CHECK_NUMBER"), "00000000")
                    CreditAmt = ds.Tables(0).Rows(j).Item("CREDIT_TRANS_AMT")
                    DebitAmt = ds.Tables(0).Rows(j).Item("DEBIT_TRANS_AMT")
                    SSN = ds.Tables(0).Rows(j).Item("SSN")
                    InsuredName = InspectString(ds.Tables(0).Rows(j).Item("INSURED"))
                    PayorName = ds.Tables(0).Rows(j).Item("PAYOR")
                    PolicyNbr = ds.Tables(0).Rows(j).Item("POLNUM")
                    DBTransCode = ds.Tables(0).Rows(j).Item("DEBIT_CODE")
                    CRTransCode = ds.Tables(0).Rows(j).Item("CREDIT_CODE")
                    CheckTransDate = ProcDate
                    CombinedCheckAmount = ds.Tables(0).Rows(j).Item("COMBINED_CHECK_AMT")

                    TmpAccount = GetGLAcctCode(Account)

                    'Valida que no haya Conseguido en el Account Code Table una cuenta de LifePRO

                    If Val(TmpAccount) = 0 Then
                        MsgBox("Error Finding LifePro Account : " & Account & " on the GL Conversion Table for Policy Number " & PolicyNbr & " Checknumber: " & CheckNumber)
                        StatusBar1.Text = "Error on Policy Number " & PolicyNbr & " Checknumber: " & CheckNumber
                        MsgBox("Error, GL File Not Created Contact the Information System Deparment")
                        'Crear log file q escriba la cuenta
                        Exit Sub
                    Else
                        Account = TmpAccount
                    End If

                    Write_GL_File()
                    'A�ade record en tabla ProcessLog
                    SQLString = "INSERT INTO ProcessLog (UserID,BatchNumber,Account,DBTransCode,CRTransCode,DbAmount,CrAmount,Insured,SSN,CheckNbr,PolicyNumber) values " & _
                                         "( '" & UserID & "'" & _
                                         ", '" & BatchNumber & "'" & _
                                         ", '" & Account & "'" & _
                                         ", '" & DBTransCode & "'" & _
                                         ", '" & CRTransCode & "'" & _
                                         ", " & DebitAmt & _
                                         ", " & CreditAmt & _
                                         ", '" & InspectString(InsuredName) & "'" & _
                                         ", '" & SSN & "'" & _
                                          ", " & Val(CheckNumber) & _
                                         ", '" & PolicyNbr & "')"

                    AccessSQLNonQueryCommand(SQLString, DBPath)

                End If 'if a�adido por modificacion en SQL Query
            Next

            oWriter.Close()
            StatusBar1.Text = "Printing Reports"

            'Crystal Report
            Dim objForm As New frmViewReport
            Dim strReportName As String
            Dim strReportPath As String
            Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo

            'Reports Path & Location
            strReportName = "ConfirmationReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If


            'Load Report
            rptDocument.Load(strReportPath)


            'To display in the forms title
            objForm.Text = "Confirmation Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface for Batchnumber: " & txtBatchNbr.Text & " '"
            rptDocument.RecordSelectionFormula = "{ProcessLog.UserID} = " & "'" & UserID & "'"
            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////

            ''Load Report 2

            'Reports Path & Location
            strReportName = "DetailReport"
            strReportPath = Application.StartupPath & "\Reports\" & strReportName & ".rpt"

            'Check if the file exists
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Unable to locate report file:" & vbCrLf & strReportPath, MsgBoxStyle.Critical + MsgBoxStyle.OKOnly, "File Not Found")
                Exit Sub
            End If
            rptDocument.Load(strReportPath)


            'To display in the forms title
            objForm.Text = "Batch Detail Report"

            'Database Location (Access -->.mdb) 
            'For Each crxDatabaseTable In rptDocument.Database.Tables
            '    crxDatabaseTable.ConnectionProperties = DBPath
            'Next crxDatabaseTable

            'rptDocument.Database.Tables(0).Location = "N:\Programmers\Accg - Fndr Data extract\Data\db1.mdb"
            'rptDocument.Database.Tables(0).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"
            'rptDocument.Database.Tables(1).Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"

            'rptDocument.Database.Tables("GLCodes").Location = "N:\Programmers\GL Interface System\bin\Data\GLACTCodes.mdb"


            'Formula Fields 
            rptDocument.DataDefinition.FormulaFields("rptTitle").Text = "'LifePro GL Interface Detail for Batch Number: " & txtBatchNbr.Text & " '"
            rptDocument.RecordSelectionFormula = "{ProcessLog.UserID} = " & "'" & UserID & "'"
            objForm.ShowDialog()

            rptDocument.Dispose()
            rptDocument.Close()
            '/////////////////////////////////////////////////////////////////////////////


            'Copia el archivo a una folder de Backup
            DestinationFile = Application.StartupPath & "\BatchFilesArchive\" & BatchNumber & ".txt"  ' Define target file name.
            FileCopy(SourceFile, DestinationFile)   ' Copy source to target.

            StatusBar1.Text = "Process Completed - GL File Created"

            Exit Sub

        Catch ex As Exception
            MsgBox("There was an error on CmdProcess_click SUB '" & (ex.Message))
            End
        Catch OLEDBex As OleDbException
            MsgBox("There was an OLE DB error '" & (OLEDBex.Message))
            End
        Catch exSQL As SqlException
            MsgBox("There was an SQL DB error '" & (exSQL.Message))
            End
        End Try

    End Sub

    Private Sub CmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdExit.Click
        Me.Close()
    End Sub

End Class
