Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMain As System.Windows.Forms.MainMenu
    Friend WithEvents mnuiExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuiDDEP As System.Windows.Forms.MenuItem
    Friend WithEvents mnuiCheck As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.mnuMain = New System.Windows.Forms.MainMenu
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuiDDEP = New System.Windows.Forms.MenuItem
        Me.mnuiCheck = New System.Windows.Forms.MenuItem
        Me.mnuiExit = New System.Windows.Forms.MenuItem
        '
        'mnuMain
        '
        Me.mnuMain.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuiDDEP, Me.mnuiCheck, Me.mnuiExit})
        Me.mnuFile.MergeType = System.Windows.Forms.MenuMerge.MergeItems
        Me.mnuFile.Text = "&File"
        '
        'mnuiDDEP
        '
        Me.mnuiDDEP.Index = 0
        Me.mnuiDDEP.Text = "Create DDEP GL File"
        '
        'mnuiCheck
        '
        Me.mnuiCheck.Index = 1
        Me.mnuiCheck.Text = "Create Check GL File"
        '
        'mnuiExit
        '
        Me.mnuiExit.Index = 2
        Me.mnuiExit.MergeOrder = 2
        Me.mnuiExit.Text = "E&xit"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(776, 414)
        Me.IsMdiContainer = True
        Me.Menu = Me.mnuMain
        Me.Name = "frmMain"
        Me.Opacity = 0
        Me.Text = "GL File Creation Application"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized

    End Sub

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim frmLoad As New FrmLogin

        For i As Double = 0 To 1 Step 0.1
            Me.Opacity = i
            Threading.Thread.Sleep(60)
        Next

        'llama a Login
        frmLoad.Owner = Me
        frmLoad.ShowDialog()

        'Disabling Menu Items
        Me.mnuiDDEP.Enabled = False
        Me.mnuiDDEP.Visible = False
        Me.mnuiCheck.Enabled = False
        Me.mnuiCheck.Visible = False

        Me.Text = "GL File Creation Application - USER: " & UserName
        Select Case SecurityLevel
            'Enable menu items depending the security level of the user
        Case "0"
                'DSER USER
                Me.mnuiCheck.Enabled = True
                Me.mnuiCheck.Visible = True
            Case "1"
                'DDEP USER
                Me.mnuiDDEP.Enabled = True
                Me.mnuiDDEP.Visible = True
            Case "2"
                'Admin
                Me.mnuiCheck.Enabled = True
                Me.mnuiCheck.Visible = True
                Me.mnuiDDEP.Enabled = True
                Me.mnuiDDEP.Visible = True
        End Select
    End Sub

    Private Sub frmMain_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        For i As Double = 1 To 0 Step -0.1
            Me.Opacity = i
            Threading.Thread.Sleep(60)
        Next
        End
    End Sub

 Private Sub mnuiDDEP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuiDDEP.Click

        Dim frmLoad As New frmDDEPImport

        'Para que no abra otro child
        If Me.MdiChildren.Length <> 0 Then
            For x As Integer = 0 To Me.MdiChildren.Length - 1
                Me.MdiChildren(x).Focus()
                frmLoad.Dispose()
                Exit Sub
            Next
        End If

        frmLoad.TopMost = False
        frmLoad.Owner = Me
        frmLoad.MdiParent = Me

        frmLoad.Show()
    End Sub

    Private Sub mnuiCheck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuiCheck.Click
        Dim frmLoad As New frmCheckGLImport

        'Para que no abra otro child
        If Me.MdiChildren.Length <> 0 Then
            For x As Integer = 0 To Me.MdiChildren.Length - 1
                Me.MdiChildren(x).Focus()
                frmLoad.Dispose()
                Exit Sub
            Next
        End If

        frmLoad.TopMost = False
        frmLoad.Owner = Me
        frmLoad.MdiParent = Me

        frmLoad.Show()
    End Sub

    Private Sub mnuiExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuiExit.Click
        For i As Double = 1 To 0 Step -0.1
            Me.Opacity = i
            Threading.Thread.Sleep(60)
        Next
        End
    End Sub
End Class
