Imports System.Data.OleDb
Public Class FrmLogin
    Inherits System.Windows.Forms.Form
    Dim LoginDBPath As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CmdOk As System.Windows.Forms.Button
    Friend WithEvents CmdCancel As System.Windows.Forms.Button
    Friend WithEvents TxtUserName As System.Windows.Forms.TextBox
    Friend WithEvents TxtPassword As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CmdOk = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TxtUserName = New System.Windows.Forms.TextBox
        Me.TxtPassword = New System.Windows.Forms.TextBox
        Me.CmdCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'CmdOk
        '
        Me.CmdOk.Location = New System.Drawing.Point(104, 112)
        Me.CmdOk.Name = "CmdOk"
        Me.CmdOk.Size = New System.Drawing.Size(72, 24)
        Me.CmdOk.TabIndex = 2
        Me.CmdOk.Text = "&OK"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "User"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password"
        '
        'TxtUserName
        '
        Me.TxtUserName.Location = New System.Drawing.Point(120, 24)
        Me.TxtUserName.Name = "TxtUserName"
        Me.TxtUserName.Size = New System.Drawing.Size(144, 20)
        Me.TxtUserName.TabIndex = 0
        Me.TxtUserName.Text = ""
        '
        'TxtPassword
        '
        Me.TxtPassword.Location = New System.Drawing.Point(120, 64)
        Me.TxtPassword.Name = "TxtPassword"
        Me.TxtPassword.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.TxtPassword.Size = New System.Drawing.Size(144, 20)
        Me.TxtPassword.TabIndex = 1
        Me.TxtPassword.Text = ""
        '
        'CmdCancel
        '
        Me.CmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdCancel.Location = New System.Drawing.Point(192, 112)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(72, 24)
        Me.CmdCancel.TabIndex = 3
        Me.CmdCancel.Text = "&Cancel"
        '
        'FrmLogin
        '
        Me.AcceptButton = Me.CmdOk
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.CmdCancel
        Me.ClientSize = New System.Drawing.Size(280, 158)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.TxtPassword)
        Me.Controls.Add(Me.TxtUserName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CmdOk)
        Me.Name = "FrmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub CmdOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdOk.Click
        Dim StrSQL As String
        Dim password As String


        'Validates fileds
        If TxtUserName.Text = "" Then
            TxtUserName.Focus()
            MsgBox("User name required", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        If TxtPassword.Text = "" Then
            TxtPassword.Focus()
            MsgBox("Password required", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        'Define SQL COnnections
        Dim AccessConnOutput As New OleDbConnection

        'Define Data Adapter
        Dim Adp As OleDbDataAdapter

        ' Define SQL Command object
        Dim AccessCmdOutput As New OleDbCommand

        ' Define Data Set 
        Dim ds As New DataSet

        Try

            ' Assign SQL Connection Strings
            AccessConnOutput.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data source=" & LoginDBPath

            AccessConnOutput.Open()
            AccessCmdOutput.Connection = AccessConnOutput

            StrSQL = "Select * from coders where coderid =" & "'" & TxtUserName.Text & "'"

            'Execute Data Adapter
            Adp = New OleDbDataAdapter(StrSQL, AccessCmdOutput.Connection)
            Adp.Fill(ds)


            'Rutina q verifica si el usuario esta creado en la base de datos
            If ds.Tables(0).Rows.Count = 0 Then
                TxtUserName.Focus()
                MsgBox("Coder is Not Valid", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            UserID = Trim(ds.Tables(0).Rows(0).Item("CoderId"))
            UserName = Trim(ds.Tables(0).Rows(0).Item("CoderName"))
            password = Trim(ds.Tables(0).Rows(0).Item("password"))
            SecurityLevel = Trim(ds.Tables(0).Rows(0).Item("SecurityLevel"))

            If Trim(TxtPassword.Text) <> password Then
                TxtUserName.Focus()
                MsgBox("Password is Not Valid", MsgBoxStyle.Exclamation)
            Else
                Me.Close()
            End If

            AccessConnOutput.Close()
            Adp = Nothing

        Catch OLEDBex As OleDbException
            MsgBox("There was an OLE Db error '" & (OLEDBex.Message))
            End

        Catch ex As Exception
            MsgBox("There was an error '" & (ex.Message))
            End
        End Try

    End Sub
    Private Sub FrmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoginDBPath = System.Configuration.ConfigurationManager.AppSettings("CashregiterDB")
        LoginDBPath = Application.StartupPath & "\data\GLACTCodes.mdb"
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        Me.Close()
        End
    End Sub
End Class
